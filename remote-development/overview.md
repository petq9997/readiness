## Summary

Remote Development is a new offering for our software-as-a-service platform that allows provisioning of remotely-managed IDEs, thereby providing a more consistent user experience compared to traditional desktop IDEs and other web IDEs. This consistency is achieved by offering a unified environment for writing code hosted in GitLab, regardless of the user's local setup. Currently, we are working on completing [Viable Maturity](https://gitlab.com/groups/gitlab-org/-/epics/9190) for Remote Development on `gitlab.com`. The focus is around bolstering reliability, extending monitoring and improving test coverage.

More user stories behind Remote Development offering can be found [here](https://gitlab.com/gitlab-org/remote-development/gitlab-remote-development-docs/-/blob/d18c3432521bb3c89d98326bf8edaa9272cfbe4b/doc/user_stories.md)

- [Blueprint](https://docs.gitlab.com/ee/architecture/blueprints/remote_development/)
- [Docs](https://gitlab.com/gitlab-org/remote-development/gitlab-remote-development-docs/-/tree/d18c3432521bb3c89d98326bf8edaa9272cfbe4b/)

- [X] What metrics, including business metrics, should be monitored to ensure will this feature launch will be a success?
  - We have some metrics to aid us in monitoring the production deployment of Remote Development. These include 
    - grafana dashboards to track API/DB metrics [here](https://dashboards.gitlab.net/d/stage-groups-ide/stage-groups-ide-group-dashboard?from=now-6h%2Fm&to=now%2Fm&var-PROMETHEUS_DS=Global&var-environment=gprd&var-stage=main&orgId=1)
    - sentry for tracking alerts [here](https://new-sentry.gitlab.net/organizations/gitlab/issues/?environment=gprd&project=3&query=feature_category%3Aremote_development)
    - logging to investigate / debug potential issues in detail [here](https://log.gprd.gitlab.net/app/discover#/?_a=(columns:!(json.status,json.method,json.meta.caller_id,json.meta.feature_category,json.path,json.request_urgency,json.duration_s,json.target_duration_s),filters:!(('$state':(store:appState),meta:(alias:!n,disabled:!f,field:json.status,key:json.status,negate:!f,params:(gte:500,lte:!n),type:range),query:(range:(json.status:(gte:500,lte:!n)))),('$state':(store:appState),meta:(alias:!n,disabled:!f,index:'7092c4e2-4eb5-46f2-8305-a7da2edad090',key:json.meta.feature_category,negate:!f,params:(query:remote_development),type:phrase),query:(match_phrase:(json.meta.feature_category:remote_development)))),grid:(columns:(json.meta.feature_category:(width:175),json.time:(width:235))),index:'7092c4e2-4eb5-46f2-8305-a7da2edad090',interval:auto,query:(language:kuery,query:''),sort:!(!(json.time,desc)))&_g=(filters:!(),refreshInterval:(pause:!t,value:0),time:(from:now-7d,to:now)))

### High-level architecture

#### System design

![Remote Development Architecture](./architecture.png)

#### Remote development with the GitLab Agent for Kubernetes topology

![Topology](./topology.png)

#### High-level overview of the communication between Rails and the agent

![Communication](./communication.png)

- [X] **Describe each component of the new feature and enumerate what it does to support customer use cases**

Remote Development is accomplished using some key components. These are:

1. [Gitlab Agent (KAS + Agent)](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/)
   - This is the component that is responsible for orchestrating the entire lifecycle of a workspace built using Remote Development within a target cluster. The agent has a dedicated module for remote development that manages the reconciliation logic for all workspaces in the cluster. The agent also routinely exchanges information with the Rails service via a reconciliation API. This API allows the agent to communicate existing cluster state while receiving the latest instructions on the workspaces bound to the agent.
   - For **internal** use within GitLab, an gent has been created and is managed at present by Engineering Productivity team. However, this particular agent itself is not generally available as a part of the Remote Development feature and it is expected that customers will bring and manage their own GitLab Agents for the smooth functioning of the feature. 
2. [Gitlab instance / rails service](https://gitlab.com/gitlab-org/gitlab)
   - This is the main service that serves the core APIs used by remote development. These APIs include those used for reconciliation as well those accessed by the UI. The reconciliation API is solely used by the remote development module within Gitlab Agent.
   - Remote development logic within the rails services further interacts with additional dependencies such as 
     - Devfile gem
       - This gem generates Kubernetes manifest YAMLs from a Devfile
     - Postgres DB
       - This is used for tracking stateful information about workspaces and remote development configurations of relevant Gitlab Agents
3. [Devfile gem](https://gitlab.com/gitlab-org/remote-development/devfile-gem)
   - This is the main gem that is used for generating Kubernetes manifests from a user's devfile. The user's devfile is processed using this gem and stored in the PostgresDB. During reconciliation with the GitLab Agent, Kubernetes manifests are generated using this gem and shared with the GitLab Agent where these manifests are applied to the Kubernetes cluster and a remote development workspace is created.
4. [Gitlab Workspaces Proxy](https://gitlab.com/gitlab-org/remote-development/gitlab-workspaces-proxy)
   - Gitlab Workspaces Proxy is responsible for authentication and authorization of the workspaces running in the cluster using the GitLab APIs. The proxy is installed in a Kubernetes cluster and automatically discovers backends based on annotations on the Kubernetes Services associated with the workspace.
5. [Gitlab VSCode WebIDE fork](https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork)
   - Gitlab VSCode WebIDE fork is a fork of the vscode project that is used to power the editor in the browser for a spawned workspace. Each running workspace is deployed with its own copy of a running VSCode server.

- [X] **For each component and dependency, what is the blast radius of failures? Is there anything in the feature design that will reduce this risk?**
  Failure scenario analysis for each component
  - Gitlab agent
    - Any issues within the remote development module in Gitlab Agent will limit the impact to the agent itself. In the worst case scenarios, a running agent may be killed by the OS in case of memory leaks. In such cases, other modules running within the same agent may be affected
    - **Impact on other components**: During agent downtime, other components of the system (such as Rails service/GitLab Workspaces Proxy) should continue to operate as expected. However, the workspace info displayed in Gitlab UI will likely become stale over the duration of the downtime due to the inability of affected Agent to either report latest cluster state / execute latest operations in the cluster
    - **Impact on feature usability**: Workspace created prior to downtime should remain operable however any operation carried out during the downtime may not be executed.
  - Gitlab instance / rails service
    - To understand the blast radius of different types of failures in gitlab instance, it may be useful to identify the group of functionality affected:
      - **Failure in Workspace management APIs** 
      - **Failure in reconciliation API**
      - Instance downtime due to external factors will affect the functionality of both workspace management and reconciliation APIs
    - Failure in workspace management APIs will have no affect on other components as they will continue to operate and serve existing workspace sessions. However, these may affect the workspaces UI making it difficult to create/list/terminate workspaces amongst other operations.
    - Failure in reconciliation API may result in Gitlab agent being unable to report the latest state of the workspaces. Additionally, agent may be unable to receive and act on latest user operations during such failures.
    - Workspaces created prior to instance downtime will be affected as the Gitlab Workspaces Proxy relies on the authentication/authorization APIs in the rails service. Any existing connections / attempts to establish new connections to these workspaces will fail given this dependency.
    - Instance downtime will further prevent the user from being able to create new workspaces or change the state of existing workspaces over the UI.
  - Devfile gem
    - Devfile gem is called from within the rails service when creating/reonciling workspaces i.e. workspace management APIs and workspace reconciliation APIs
    - **Impact on other components**: Failures in devfile gem should only affect workspace management APIs and workspace reconciliation APIs within the rails service especially those related to creation / reonciliation of workspaces. Other components of the system should not be affected
  - Gitlab Workspaces Proxy
    - **Impact on other components**: Failures in the workspace proxy should have no impact on other components in the system
    - **Impact on feature usability**:
      - There should be no impact on the ability of a user to create/update workspaces given that these rely on the stability of Rails service, devfile-gem and Gitlab Agent.
      - However, users will face issues while trying to access their workspaces.
  - Gitlab VSCode WebIDE fork
    - **Impact on other components**: Failures in the Gitlab VSCode WebIDE fork should have no impact on other components in the system
    - **Impact on feature usability**:
      - There should be no impact on the ability of a user to create/update workspaces given that these rely on the stability of Rails service, devfile gem, GitLab agent.
      - However, users may face issues when trying to access new workspace sessions that use the version of the editor with the error. These errors may result in the crashing of affected workspaces. However, workspace sessions using editor versions without the error will remain unaffected
### User FLow

![Remote Development User Flow](./user_flow.png)

## Operational Risk Assessment

### Workspace Specific Risks

- [x] **What are the potential scalability or performance issues that may result with this change?**

  - Resource consumption: Each workspace requires system resources, including CPU, memory, and storage. As more workspaces are created, the demand for these resources will increase, potentially leading to scalability and performance issues if there are not enough resources available to support the workload in the Kubernetes cluster.
  - Networking: The remote development workflow relies heavily on network connectivity. If network latency or bandwidth constraints are present, it may result in degraded performance or connection issues.
  - Data transfer: As users connect from their local machine to the workspaces, there may be significant data transfer between the two. This can also impact performance if the data transfer is not optimized or if there are network constraints.

- [x] **List the top three operational risks when this feature goes live.**

  - Resource constraints: As the number of workspaces and users grows, there may be resource constraints in the remote development Kubernetes cluster.
  - Network issues: Network connectivity issues could impact the ability to use the workspace feature.
  - System availability: GitLab Workspaces relies on the availability of multiple systems - GitLab instance, GitLab Agent for Kubernetes, Kubernetes cluster, stability of the injected editor(Gitlab VSCode WebIDE fork), etc. If any system experiences downtime or other availability issues, it may impact the ability to create or use workspaces.

- [x] **What are a few operational concerns that will not be present at launch, but may be a concern later?**

  - Integration issues: As more users and tools are added to the workspace environment, there may be integration issues between different tools or systems.

- [x] **Document every way the customer and admins will interact with this new feature and how they will be impacted by a failure of each interaction.**

  - **Creating a workspace:** Customers can create a new workspace using the GitLab Workspaces feature. This involves selecting a project and clicking on the "Edit" button in a Project repository with a valid `devfile`. If this feature fails, customers will not be able to create new workspaces.
  - **Accessing a workspace:** Customers can access a workspace by opening it in the injected editor(Gitlab VSCode WebIDE fork) or via ssh. The workspace is a full-fledged development environment that runs in the cloud and is accessible directly from the GitLab interface. If the workspace fails to load or becomes unresponsive, customers will not be able to access their workspaces.
  - **Working inside a workspace:** Customers can use their workspaces by editing files, running commands in the terminal, and installing new software. Changes are automatically saved locally and can be committed directly from the workspace. If the workspace environment is unstable or fails, customers may lose their changes or experience data corruption.
  - **Stopping a running workspace:** A running workspace may be stopped by the user over the UI. A stopped workspace can then be resumed back into a running state by the user. Failure to stop a workspace may result in the workspace being active and consume cluster compute resources beyond intended.
  - **Restarting a running workspace:** A running workspace can be restarted by the user over the UI. A restarted workspace will first stop and then resume the workspace backing into running state. Failure to restart a workspace can affect user experience especially if the user restarted a workspace to recover from an error while using the workspace.
  - **Terminating a workspace:** A workspace may be terminated by the user over the UI, however this will not delete the workspace record from the DB. Failure to terminate a workspace can result in the workspace being active and consuming cluster resources beyond intended.
    - *Note*: Due to our cascade delete database rules, workspace records may be deleted if the associated records (i.e. projects) are deleted, but the user does not have the ability to directly delete the DB record for a workspace.
  - **Managing agents and remote development module settings:** Admins can install and configure the GitLab Agent for Kubernetes, which is necessary for the operation of the workspaces. If the agent fails or is misconfigured, it could prevent the creation or use of workspaces.
  However, it is important to note that admins do not have direct control over access to workspaces. The access control for workspaces is primarily managed through the agent configuration file, which can be created by anyone with the proper group/project permissions. The agent configuration file determines which group/project the agent is associated with, indirectly controlling access to the workspace.
  Additionally, the list of properties of remote development module that can be configured via agent configuration can be found [here](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/fec406e0f178edbb4907cfe53efd17b082d35bef/pkg/agentcfg/agentcfg_proto_docs.md#gitlab-agent-agentcfg-RemoteDevelopmentCF)

- [x] **As a thought experiment, think of worst-case failure scenarios for this product feature, how can the blast-radius of the failure be isolated?**

  - Catastrophic network failure: If there is a catastrophic network failure, customers may not be able to access their workspaces or data.
  - Data corruption: If there is data corruption while using / operating a workspace, the impact should be limited to the workspace in question. It will not affect the feature, other workspaces or any other component of the system.

### GitLab Wide Risks

- [x] **What are the potential scalability or performance issues that may result with this change?**

  - Database performance: The use of a PostgreSQL database to store workspace metadata introduces the potential for database performance issues, such as slow query times or resource contention, as the number of workspaces and the amount of data stored in the database increases.

- [x] **List the top three operational risks when this feature goes live.**

  - Database performance issues: The PostgreSQL database used to store workspace metadata could experience performance issues as the number of workspaces and data stored in the database grows.
  - Compliance issues: Depending on the industry or regulatory environment, there may be compliance issues related to the use of remote development environments.
  - Security breaches: There is a risk of security breaches, e.g. inadvertently exposing secrets either on the Kubernetes cluster or in the communication between the GitLab monolith and agent/Kubernetes.

- [x] **Can the new product feature be safely rolled back once it is live, can it be disabled using a feature flag?**

  - GitLab Workspaces ships with a feature flag that is enabled by default. It can be disabled, however there are some points that should be noted:
    - this will not result in an immediate teardown of running workspaces that were created prior to disabling the feature
    - since the feature flag exists only in the rails instance, and not every user action in the Gitlab VSCode WebIDE fork may result in an API call to Gitlab Workspaces Proxy (and then to rails where an error may be returned), it may still be possible to work within a running workspace even after the feature flag is disabled

  We have created [an issue](https://gitlab.com/gitlab-org/gitlab/-/issues/425754) to investigate how this behavior can be better addressed in the future.


- [x] **List the external and internal dependencies to the application (ex: redis, postgres, etc) for this feature and how the service will be impacted by a failure of that dependency.**

  - PostgreSQL database: This database is used to store workspace metadata. A failure of this dependency could impact the ability to create, use, or delete workspaces.
  - Cloud(GCP/AWS/Azure/etc) and data centers of customers - Volumes used for each workspace are provisioned dynamically from Kubernetes using cloud services for block storage. Any downtime of the ability to create/modify/delete volumes would affect the availability of the application. 
  - Kubernetes Cluster: The Kubernetes cluster is used to orchestrate and manage the containers for the workspaces. Any issues with the Kubernetes cluster could impact the availability and functionality of the Remote Development feature.
  - GitLab Agent for Kubernetes: The GitLab Agent for Kubernetes is used to connect the GitLab instance with the Kubernetes cluster. If the GitLab Agent for Kubernetes fails, it could impact the ability to create or update workspaces.
  - Ingress Controller within Kubernetes Cluster: This is the entry point for all traffic when a workspace is accessed by a user. Any issue with the ingress controller or its configuration would result in the users not being able to access / work within the workspaces as expected.

## Database

See https://gitlab.com/gitlab-org/remote-development/gitlab-remote-development-docs/-/blob/d18c3432521bb3c89d98326bf8edaa9272cfbe4b/doc/workspace-db-design.md for more information.

- [x] **If we use a database, is the data structure verified and vetted by the database team?**

  - Yes, the team has designed and verified the database structure for the remote development feature.

- [x] **Do we have an approximate growth rate of the stored data (for capacity planning)?**

  - Due to the current state of the feature and the absence of audit logs, there is no real potential for ongoing growth of workspace-related data in the database. The largest amount of data would be the processed devfiles and potentially larger variables, but these do not grow over time. As for audit logs, we do not have a precise estimate of their growth until they are implemented.

- [x] **Can we age data and delete data of a certain age?**

  - We currently do not have a specific data retention policy in place for workspace-related data. Once workspaces are terminated, they remain in the database indefinitely unless they are deleted by a cascade-delete rule.

## Security and Compliance

- **Are we adding any new resources of the following type? (If yes, please list them here or link to a place where they are listed)**
  - [ ] **AWS Accounts/GCP Projects**
  - [ ] **New Subnets**
  - [ ] **VPC/Network Peering**
  - [ ] **DNS names**
  - [x] **Entry-points exposed to the internet (Public IPs, Load-Balancers, Buckets, etc...)**
    - While not currently required, as we work towards supporting SaaS-hosted workspaces, we may need to consider additional infrastructure such as public IPs, load balancers, or storage volumes. These potential future needs will be evaluated and implemented following GitLab's security and compliance guidelines. See [Remote Development Complete Maturity • GitLab Managed Workspaces](https://gitlab.com/groups/gitlab-org/-/epics/10637) for more information.
  - [x] **Other (anything relevant that might be worth mention)**
    - Block storage volumes will be dynamically provisioned for each workspace from Kubernetes using Persistent Volume Claims.
    - A PostgreSQL database will be used to store workspace metadata.

- **Secure Software Development Life Cycle (SSDLC)**
    - [ ] **Is the configuration following a security standard? (CIS is a good baseline for example)**
    - [ ] **All cloud infrastructure resources are labeled according to the [Infrastructure Labels and Tags](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/) guidelines**
    - [x] **Were the [GitLab security development guidelines](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html) followed for this feature?**
      - Yes.
    - [x] **Do we have an automatic procedure to update the infrastructure (OS, container images, packages, etc...)**
      - No, currently the GitLab Remote Development feature does not have an automatic procedure to update the infrastructure such as the Operating System, container images, or packages. This is something that needs to be managed manually by the administrators.
    - [x] **Do we use IaC (Terraform) for all the infrastructure related to this feature? If not, what kind of resources are not covered?**
      - Yes, Remote Development feature on GitLab.com uses Terraform for managing all of the different components of its infrastructure.
        - [x] **Do we have secure static code analysis tools ([kics](https://github.com/Checkmarx/kics) or [checkov](https://github.com/bridgecrewio/checkov)) covering this feature's terraform?**
          - The secure static code analysis tools are being used for the following components of Remote Development system:
            - Rails service
            - KAS
            - GitLab Agent (maintained by Engineering Productivity)
  - **If there's a new terraform state:**
    - [x] **Where is to terraform state stored, and who has access to it?**
      - Remote Dev system relies on GitLab Rails service and KAS which is Gitlab-managed. Additionally, the internal Gitlab Agent(along with its terraform state) for Remote Development is maintained by Engineering Productivity team([repository](https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/terraform)).
  - [x] **Does this feature add secrets to the terraform state? If yes, can they be stored in a secrets manager?**
    - No new secrets have been added as a part of the Remote Development feature to the Terraform state
    - However, a GitLab agent has been introduced for the internal use of Remote Development feature by GitLab team members only and the secrets associated with this internal agent/cluster are managed using Google Secrets Manager([repo](https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/blob/172ef4e040cce7d321559b6464fb99966906fba8/remote-development/secrets.tf)).
  - **If we're creating new containers:**
    - [x] **Are we using a distroless base image?**
      - Currently, we are not using a distroless base image for the containers in the Remote Development feature. We are using standard base images that are commonly used in the industry:
        - for GitLab Workspaces Proxy: golang:1.19
        - for Project cloner: alpine/git:2.36.3 
        However, we are considering the use of distroless images in the future to minimize the attack surface of our containers ([relevant issue](https://gitlab.com/gitlab-org/gitlab/-/issues/425533)).
    - **Do we have security scanners covering these containers?**
      - [x] **`kics` or `checkov` for Dockerfiles for example**
        - We do not currently use `kics` or `checkov` for Dockerfile scanning in the Remote Development feature. However, we are considering the use of these tools in the future to improve our security posture([relevant issue](https://gitlab.com/gitlab-org/gitlab/-/issues/425536)).
      - [x] **[GitLab's container](https://docs.gitlab.com/ee/user/application_security/container_scanning/#configuration) scanner for vulnerabilities**
        - Yes, we use GitLab's built-in container scanning feature to scan the containers used in the Remote Development feature for vulnerabilities. This helps us to identify and mitigate potential security issues in our containers. See [our recent vulnerabilities as examples](https://gitlab.com/groups/gitlab-org/-/issues/?sort=due_date_desc&state=all&label_name%5B%5D=security&label_name%5B%5D=group%3A%3Aide&label_name%5B%5D=vulnmapper&first_page_size=20)

- **Identity and Access Management**
  - [x] Are we adding any new forms of **Authentication** (New service-accounts, users/password for storage, OIDC, etc...)?
    - Yes, for each workspace created, a Personal Access Token (PAT) is generated with `write_repository` permisssions and stored in the database. This PAT is encrypted with the standard database instance-wide encryption secret key and is used to authenticate and authorize access to workspace variables, which are then sent to the agent during reconciliation.
    When a user attempts to access a running workspace, they have to do so via OAuth flow. Once inside a workspace, the injected PAT can then also be used for performing any git operations. 
    **Note:** The PAT is also stored in Kubernetes as a Secret in the associated workspace's namespace. See [Create workspaces from private repositories](https://gitlab.com/groups/gitlab-org/-/epics/10882) for more information.
    - Additionally, Remote Development now supports SSH capability. Users can use their local VS Code instance to access their remote workspace environment by specifying their workspace name and the location of the Gitlab Workspaces Proxy. Upon connection, VS Code will prompt for a password, which can be a Personal Access Token (PAT) generated from the GitLab UI. See [SSH Phase 1: Support access from Local VS Code using PAT](https://gitlab.com/groups/gitlab-org/-/epics/10908).
  - [x] **Does it follow the least privilege principle?**
    - Yes, the system follows the least privilege principle. The PAT is stored in an encrypted format in the database and is sent along with other workspace variables to the agent during full reconciliation (of all workspaces or a specific workspace) or when a new workspace is created. This ensures that only the necessary information is accessible at any given time. Additionally, the workspace's PAT is stored in Kubernetes as a Secret in the associated workspace's namespace, limiting access to this sensitive information.
    - For the SSH capability, users only need to provide a PAT, which can be scoped to limit its permissions. The connection is made directly to their specific workspace, following the least privilege principle.

- **If we are adding any new Data Storage (Databases, buckets, etc...)**
  - [x] **What kind of data is stored on each system? (secrets, customer data, audit, etc...)**

    - The volume on each workspace in the Kubernetes cluster would contain user's data which can be personal.
    - For the Rails monolith, we are not adding any new databases or buckets. However, we do use the existing PostgreSQL database to store some workspace-related data.
    - In the Kubernetes cluster where the agent is running, various Kubernetes resources are created for secrets, data volumes, and other related data storage needs. These resources are used to manage the workspace environment and facilitate the remote development feature.

  - [x] **How is data rated according to our [data classification standard](https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html) (customer data is RED)**

    - The data stored in the existing PostgreSQL database for workspace metadata and the block storage volumes dynamically provisioned for each workspace in Kubernetes are considered customer data, and therefore, they are classified as RED.

  - [x] **Is data it encrypted at rest? (If the storage is provided by a GCP service, the answer is most likely yes)**

    - The data stored in the block storage volumes dynamically provisioned for each workspace in Kubernetes is encrypted at rest. The encryption is managed by the cloud service provider.
    - For the data stored in the PostgreSQL database in the Rails monolith, we use the standard `attr_encrypted` approach for encrypting some data at rest, just like many other existing mono\\\lith features.

  - [x] **Do we have audit logs on data access?**

    - The access to the PostgreSQL database is logged in the Rails monolith and access to the DB console is audited using Teleport.

- **Network security (encryption and ports should be clear in the architecture diagram above)**
  - [ ] **Firewalls follow the least privilege principle (w/ network policies in Kubernetes or firewalls on cloud provider)**
  - [x] **Is the service covered by any DDoS protection solution (GCP/AWS load-balancers or Cloudflare usually cover this)**

    - For the GitLab monolith and all API access to it, we rely on the standard DDoS protection in place for the hosted SaaS instance. As for the customer's Kubernetes clusters where they run agents, we do not have control or knowledge over what DDoS protection they might have in place.

  - [x] **Is the service covered by a WAF (Web Application Firewall)**

    - KAS on GitLab.com is covered by WAF. Since the module in GitLab Agent does not accept any incoming HTTP/HTTPS traffic, there is no need for WAF here.

- **Logging & Audit**
  - [x] **Has effort been made to obscure or elide sensitive customer data in logging?**

    - We are consciously aware of what type of data we allow into the logs and avoid logging anything which might be sensitive (i.e. contents of devfiles and variables), although we don't explicitly scrub anything. Additionally, we plan to revisit and review this in the future to ensure that the code conforms to the existing standards([issue](https://gitlab.com/gitlab-org/gitlab/-/issues/409360)). 

- **Compliance**
  - [x] **Is the service subject to any regulatory/compliance standards? If so, detail which and provide details on applicable controls, management processes, additional monitoring, and mitigating factors.**

    - GitLab Remote Development uses mutual TLS (mTLS) authentication to secure communication between components, and each component runs with limited privileges to reduce the impact of potential vulnerabilities. Access to the system can be restricted to authorized users using GitLab's built-in access controls. Users can be granted permissions to specific projects and environments, and access can be further restricted through the use of namespaces and labels.

    - In addition to these security measures, GitLab Remote Development includes mechanisms for monitoring and logging system activity. For example, logs are generated for each component and can be aggregated and analyzed using tools like Elasticsearch and Kibana. The system also includes health checks and liveness probes to monitor the status of each component.

    - The security and access control measures described outlined [here](https://gitlab.com/gitlab-org/remote-development/gitlab-remote-development-docs/-/blob/d18c3432521bb3c89d98326bf8edaa9272cfbe4b/doc/securing-the-workspace.md) can help meet a variety of compliance requirements. For example, mTLS authentication and access controls can help meet requirements for data privacy and protection. The monitoring and logging capabilities of the system can also be used to demonstrate compliance with requirements for auditing and reporting if needed.

## Performance

- [x] **Explain what validation was done following GitLab's [performance guidelines](https://docs.gitlab.com/ee/development/performance.html). Please explain which tools were used and link to the results below.**
  - The validation process for the Remote Development feature is still ongoing. We plan to use the GitLab Performance Tool for load testing, which is based on the open-source load testing tool k6. The load test scenario for creating workspaces will be defined in the create_workspaces.js file in the performance/load_scenarios directory of the GitLab Performance Tool repository. The results of these tests are not available as these have not been implemented yet ([relevant issue](https://gitlab.com/gitlab-org/gitlab/-/issues/425517)).
- [x] **Are there any potential performance impacts on the database when this feature is enabled at GitLab.com scale?**
  - Yes, there could be potential performance impacts on the database. The Remote Development feature uses a PostgreSQL database to store workspace metadata. As the number of workspaces and the amount of data stored in the database increases, there could be database performance issues, such as slow query times or resource contention.
  - However, all changes/MRs that impact the database have gone through the standard Database review which involves providing an EXPLAIN plan for the queries being used
  - The user-facing APIs are also subject to the normal GitLab API rate limiting, which should also serve as a protection against potential excessive database read impact
  - Furthermore, there are plans in place to implement load testing for this feature which should help in identifying potential performance issues/bottlenecks 
- [x] **Are there any throttling limits imposed by this feature? If so how are they managed?**
  - Currently, there are no specific throttling limits imposed by the Remote Development feature. However, the feature is subject to the general rate limits imposed by GitLab's API.
- [x] **If there are throttling limits, what is the customer experience of hitting a limit?**
  - As mentioned above, the Remote Development feature does not impose specific throttling limits. If a user hits the general rate limits imposed by GitLab's API, they would receive a `429 Too Many Requests` response.
- [x] **For all dependencies external and internal to the application, are there retry and back-off strategies for them?**
  - No, Remote Development does not include any explicit retry and back-off strategies for its dependencies. However, the core of Remote Development system is based on a reconciliation loop between the Rails service and GitLab Agent which triggers periodically to synchronize changed data across the two. Furthermore, this reconciliation loop will also attempt to synchronize all of the relevant data(including unchanged data) from time to time albeit at a much lesser frequency. So if there are any short-lived errors due to temporary outages/network issues, the associated operations will be retried eventually as the system will try to recover and converge to desired states. In other words, there is no explicit logic to retry failed operations but the system will attempt to recover eventually given this reconciliation process. The reconciliation loop is really the only place where this is relevant (i.e. where there is an HTTP-level dependency), and the frequency of that is gated by the full/partial reconciliation intervals, which will usually be set at multi-second or multi-minute granularity.
- [x] **Does the feature account for brief spikes in traffic, at least 2x above the expected TPS?**
  - The potential impact can be analyzed by going over the different scenarios that may result in spikes in traffic:
    - **User actions in the GitLab UI**
      - We expect users to spend more time working on code within a running workspace and less time creating new workspaces / modifying existing workspaces. As such we expect this type of traffic to be low.
      - This traffic affects Rails service and the database. Given the low expected TPS for such scenarios, the service / DB should be able to handle spikes in traffic.
    - **Reconciliation traffic between the Rails service and connected GitLab Agent(s)**
      - Reconciliation between Rails service and connected GitLab Agent(s) is expected to be consistent and execute periodically in configured intervals
      - In theory, Rails service may experience peak traffic in some rare cases where reconciliation for each agent occurs at exactly the same instance of time
      - However, since each Agent makes only 1 API call per reconciliation cycle, this peak traffic will scale linearly with the number of connected Agents and should be low given that we do not expect a large number of connected agents.
    - **User actions within a running workspace**
      - This is expected to be the most common scenario when using Remote Development feature
      - Most of the traffic here is expected to be self-contained within the workspace itself and should not hit any of the new APIs/tables introduced in the Rails service.
    Note: The above points are based on an understanding of how the feature is expected to behave. Once load testing is in place, bottlenecks can be discovered and shall be addressed accordingly.

## Backup and Restore

- [x] **Outside of existing backups, are there any other customer data that needs to be backed up for this product feature?**

  - No. There will be no data persistance provided for Workspaces initially. GitLab's Remote Development feature does not provide data persistence for workspaces. This means that when a workspace is terminated, all data within that workspace is lost and not backed up.

- [x] **Are backups monitored?**

  - No.

- [x] **Was a restore from backup tested?**

  - No.

## Monitoring and Alerts

- [x] **Is the service logging in JSON format and are logs forwarded to logstash?**
    - Yes, we do currently have some logging to a json-format log in `remote_development.log`, as well as some other logging which goes to the standard rails log in non-json format.
- [x] **Is the service reporting metrics to Prometheus?**
    - Metrics are reported only for the following components of the Remote Development system:
      - Rails service ([here](https://dashboards.gitlab.net/d/stage-groups-ide/stage-groups-ide-group-dashboard?from=1697085257350&to=1697085557350&var-PROMETHEUS_DS=Global&var-environment=gprd&var-stage=main&orgId=1&project=3&query=feature_category%3Aremote_development))
- [x] **How is the end-to-end customer experience measured?**
    - The end-to-end customer experience is not currently measured in a systematic way. However, feedback from users and bug reports are used to understand and improve the user experience. See [🗣 Remote Development | Feedback & Insights](https://gitlab.com/groups/gitlab-org/-/epics/8882).
- [x] **Do we have a target SLA in place for this service?**
    - No, there is currently no target Service Level Agreement (SLA) in place for the GitLab Remote Development feature.
- [x] **Do we know what the indicators (SLI) are that map to the target SLA?**
    - As there is no target SLA, there are no Service Level Indicators (SLIs) defined.
- [x] **Do we have alerts that are triggered when the SLI's (and thus the SLA) are not met?**
    - As there are no SLIs or SLA, there are no alerts set up for this. However, as alerts will still be useful for discovery of issues, there is an [epic](https://gitlab.com/groups/gitlab-org/-/epics/10599) to add these in the future.
- [x] **Do we have troubleshooting runbooks linked to these alerts?**
    - As there are no alerts, there are no linked troubleshooting runbooks.
- [x] **What are the thresholds for tweeting or issuing an official customer notification for an outage related to this feature?**
    - There are currently no defined thresholds for issuing customer notifications related to outages of this feature.
- [x] **Do the on-call rotations responsible for this service have access to this service?**
    - Currently, specific on-call rotations for the GitLab Remote Development feature have not been established.

## Responsibility

- [x] **Which individuals are the subject matter experts and know the most about this feature?**

  - @ericschurter 
  - @oregand
  - @vtak
  - @shekharpatnaik

- [x] **Which team or set of individuals will take responsibility for the reliability of the feature once it is in production?**

  Below is our understanding of the teams responsible and the scope of responsibilities that will be undertaken by them:
  - Create:IDE
    - Responsible for monitoring the error budget of the Remote Development system. A dedicated SRE will soon be hired to join the team and take over this responsibility
    - Responsible for following up on and resolving issues that require modifying components of Remote Development system especially if there may be an impact on the business logic. These issues could be the consequence of the post-mortem of a live incident or identified as essential to managing the error budget of the system
  - Engineering Productivity
    - Engineering Productivity currently manages a cluster for Remote Development workspaces for internal use within GitLab. Specifically, this includes a GKE cluster with a running GitLab Agent. Therefore, Engineering Productivity team will be responsible for managing issues related to this part of the infrastructure.
    - In the future, this part of the Remote Development system may be handed over to Infrastructure team and may require another review of these responsibilities
  - Infrastructure
    - Responsible for managing infrastructure issues related to other components used by Remote Development system i.e.
      - GitLab Rails service
      - KAS
    - Examples of these issues include scaling up of resources etc

- [x] **Is someone from the team who built the feature on call for the launch? If not, why not?**

  - @oregand
  - @vtak
  - @shekharpatnaik

## Testing

- [x] **Describe the load test plan used for this feature. What breaking points were validated?**
  - **Define Load Profile:** Define the load profile for the test. This should include the number of workspaces (N) to be created across the number of groups/projects (M). For starters, the plan is use the following values:
    - N: 5000 workspaces
    - M: Since it is expected that there will be around 10-100 workspaces per project, N can be split across 50-500 projects
  - **Create Workspaces:** Utilize a load testing tool like the GitLab Performance Tool to automate the creation of N workspaces across M groups/projects. This can be achieved by defining a load test scenario in a new file `create_workspaces.js` in the `performance/load_scenarios` directory of the GitLab Performance Tool repository. The scenario should simulate the creation of workspaces using GitLab's API ([relevant issue](https://gitlab.com/gitlab-org/gitlab/-/issues/425517)).
    - **Success:** All workspaces are created successfully without any errors. The success of the operation can be verified by checking the response status and the creation of the workspace in the response body using the `check` function from the k6 library.
    - **Failure:** Any error occurs during the creation of workspaces.
  - Likely breakpoints for the GitLab Remote Development feature under load 
    -  Resource Constraints
    -  Database Performance
    -  Network Bottlenecks
    -  Storage I/O
    -  API Rate Limiting

To conduct a load test, we will utilize the [GitLab Performance Tool](https://gitlab.com/gitlab-org/quality/performance). We can create a test scenario in the `performance/load_scenarios` directory of the GitLab Performance Tool repository. See the GraphQL API Docs [here](https://docs.gitlab.com/ee/api/graphql/reference).

1. Environment Preparation: Follow the instructions in the [Environment Preparation Guide](https://gitlab.com/gitlab-org/quality/performance/-/blob/master/docs/environment_prep.md) to set up your testing environment. 
2. Create a new scenario file: In the `performance/load_scenarios` directory, create a new file called `create_workspaces.js`.
3. Define the scenario: In the `create_workspaces.js` file, define the scenario for creating workspaces.

```javascript
// performance/load_scenarios/create_workspaces.js

import { check, group } from 'k6';
import { Rate } from 'k6/metrics';
import http from 'k6/http';
import { gitlabUrl, gitlabToken } from '../config.js';

export let errorRate = new Rate('errors');

export default function () {
    group('Create Workspaces', function () {
        let headers = {
            'PRIVATE-TOKEN': gitlabToken,
            'Content-Type': 'application/json',
        };

        // Replace with a group and project ID
        let groupID = 1;
        let projectID = 1;

        // Create a workspace
        let res = http.post(
            `${gitlabUrl}/api/graphql`,
            JSON.stringify({
                query: `
                    mutation {
                        workspaceCreate(input: {
                            clientMutationId: "workspace-${__VU}-${__ITER}",
                            clusterAgentId: "your-cluster-agent-id",
                            desiredState: "running",
                            devfilePath: "path-to-devfile",
                            devfileRef: "master",
                            editor: "webide",
                            maxHoursBeforeTermination: 24,
                            projectId: "${projectID}"
                        }) {
                            workspace {
                                id
                            }
                            errors
                        }
                    }
                `,
            }),
            { headers: headers }
        );

        // Check the response
        let result = check(res, {
            'status is 200': (r) => r.status === 200,
            'workspace was created': (r) => JSON.parse(r.body).data.workspaceCreate.workspace !== null,
        });

        errorRate.add(!result);
    });
}
```

4. Run the load test: You can run the load test using the k6 command. Here's an example command:
js

```shell
k6 run -u 10 -d 30s performance/load_scenarios/create_workspaces.js
```

This command will run the `CreateWorkspaces` scenario with 10 virtual users for 30 seconds.

- [x] **For the component failures that were theorized for this feature, were they tested? If so include the results of these failure tests.**

  - No, the component failures that were theorized for this feature have not been tested yet.

- [x] **Give a brief overview of what tests are run automatically in GitLab's CI/CD pipeline for this feature?**

  - The GitLab CI/CD pipeline for the Remote Development feature includes a variety of tests to ensure the reliability and correctness of the feature. These tests are divided into two main categories: unit tests and end-to-end tests.
    - Unit Tests:
      - The unit tests are designed to test individual components of the Remote Development feature in isolation. These tests are mock-based, meaning they simulate the behavior of external dependencies. The unit tests cover both the Service layer and the Domain Logic layer of the feature.
    - Service Layer Unit Tests: These tests ensure that the service layer of the Remote Development feature behaves as expected. They test the standard API of the domain classes, including argument passing and response handling.
    - Domain Logic Layer Unit Tests: These tests focus on the domain logic of the Remote Development feature. They test the return values and error handling of the domain logic components.
  - End-to-End Tests:
    - The end-to-end tests are designed to test the Remote Development feature as a whole, from start to finish. These tests simulate a user going through the "happy path" scenario of the feature. See [Implement E2E test that covers happy path in the Workspaces UI](https://gitlab.com/gitlab-org/gitlab/-/issues/408389) and [Support end-to-end QA test in CI for Workspaces](https://gitlab.com/gitlab-org/gitlab/-/issues/397005). 
    **Note:** There are plans to add more tests in upcoming releases to increase coverage such as
      - test case to open a running workspace and start an app ([issue](https://gitlab.com/gitlab-org/gitlab/-/issues/420316))
      - test case to verify error handling/reporting under different scenarios ([issue](https://gitlab.com/gitlab-org/gitlab/-/issues/421669))
  - Frontend Component Tests:
    - The frontend component tests focus on testing the Vue components used in the Remote Development feature. These tests validate the behavior and functionality of individual Vue components, including props, events, slots, styles, classes, and lifecycle hooks. The tests interact with the components as a user would, simulating user actions and asserting the expected output
    
    These tests ensure that the entire flow of the Remote Development feature works as expected, from the user interface down to the underlying services and domain logic. All of these tests are run automatically as part of GitLab's CI/CD pipeline whenever changes are made to the Remote Development feature. This helps to catch any potential issues or regressions early in the development process.
