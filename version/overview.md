# version.gitlab.com migration to GKE / Auto DevOps

[version.gitlab.com](https://version.gitlab.com) is an application that tracks self hosted usage statistics.

https://gitlab.com/gitlab-services/version-gitlab-com

This readiness review is for migrating the existing services from where it is hosted on Amazon Web Services, into a GKE cluster which accepts deployments from Auto Devops

**Production Change Issue** - https://gitlab.com/gitlab-com/gl-infra/production/issues/1231

## Table of contents

  * [Architecture overview](#architecture-and-overview)
  * [Configuration](#configuration)
  * [Risk assessment](#risk-assessment-and-blast-radius-of-failures)
  * [Security considerations](#security-considerations)
  * [Application upgrade/rollback](#application-upgrade-and-rollback)
  * [Observability and Monitoring/Logging](#observability-and-monitoring)
  * [Testing](#testing)

## Architecture and Overview

The production GKE cluster is created and managed along with its project, by the https://ops.gitlab.net/gitlab-com/services-base CI jobs. There is a tracking branch called `gs-production` and environment at https://ops.gitlab.net/gitlab-com/services-base/environments which is associated with this project.  The GKE cluster is attached to the [Gitlab Services](https://gitlab.com/gitlab-services) group name space, using GitLab's Kubernetes integration. This allows Auto DevOps to deploy to this cluster when targeting the `production` environment.

The staging cluster is provisioned in a similar way, using a separate GCP project, and tracking branch. Review apps are also deployed to the staging cluster. 

### Diagram Examples

* No Drawings yet

### High level overview

* The production database uses a cloudSQL instance which is configured in the project, and managed by terraform from the [services-base] https://ops.gitlab.net/gitlab-com/services-base) repository. Any project in the [Gitlab Services](https://gitlab.com/gitlab-services) group which needs access to this database (including this one), needs a `DATABASE_URL` environment variable set and associated with the `production` environment.
* The staging and review databases run in pods, and are not seeded with any data beyond what is in the project repository.

---

## Configuration

Application configuration is primarily managed via Environment variables in the project settings. https://gitlab.com/gitlab-services/version-gitlab-com/-/settings/ci_cd

The following variables are used:

* DATABASE_URL - The connect string for the database
* DB_INITIALIZE - The command use to initialize the database
* DB_MIGRATE - The command to run migrations
* POSTGRES_ENABLED (production) - Disables using pods for production database
* REPLICAS (production) - Starting with 4 pods and adjusting according to traffic and load
* TEST_DISABLED - Currently rails tests don't pass. Once Auto Devops is enabled, the tests should be switched back on.

## Risk Assessment and Blast radius of failures

The version service is deployed in one or more pods on nodes
that span at least three availability zones in a single region. The version service is also deployed into a GCP project name space which is separate from the main GitLab application. 

If the version service becomes unavailable, there will be no impact on GitLab.com.  However, any self managed instances which are sending usage statistics during the outage will fail to have their data recorded.  Most of this data is sent on a weekly schedule every Sunday.


## Resource Management

We currently have no historical data on resource usage of the existing environment.  We have chosen resource sizing based on the size of the existing instance.  Once this is moved into a more standardized environment, it will be easier to tune the cluster size.  For now the cluster needs to be oversized, since we are planning to put more applications into it (starting with `license`).

Current sizing is 8 CPU, 30G memory, 2x80G disk
Current usage is 10% CPU idle, 20% CPU Sundays, 60% CPU peak - 18%-20% memory usage


## Security Considerations

### Network Access

All incoming network access is through the Kubernetes ingress which is provisioned by helm via the Kubernetes integration page. Certificates are managed via the `certmanager` instance, also configured via the integration page.

Currently, the IP address has to be manually copied from the ingress field on the integration page, into the DNS A record on Route53.  There is some [ongoing work](https://ops.gitlab.net/gitlab-com/services-base/merge_requests/23) to automate this, but it won't be ready in time for the initial production move.


## Application Upgrade and Rollback

Application upgrade and Rollback will be handled entirely by the built in Auto DevOps tools.

## Dogfooding

One of the primary drivers for moving this application to Kubernetes is to create an environment for Dogfooding the Auto DevOps functionality within the product. The idea is to use the default pipeline, without the use of a `.gitlab-ci.yml` file.

Wherever possible we try to leverage GitLab application features for managing
the deployments on Kubernetes. This includes:

* AutoDevops
* CI pipelines
* Package registry


## Observability and Monitoring

Since the purpose of moving this application to Auto DevOps is to dogfood the product and the internal tools, we have intentionally not set up additional monitoring. This may prove to be insufficient, but the plan is to use these gaps as data to build out better monitoring tools within GitLab itself.

## Testing

Since this is set up with Auto DevOps, all pushes to production are automatically pushed to staging first.  All MR's are deployed to a Review app before being merged to master and pushed to staging.  So each change is forced through two additional deployments, and running the application's automated test suite two times before it's possible to push to production.

### Load testing

No separate load testing was done for this application. Since it has been running in production for a long time we already have some data on how it has been performing.  The dashboard is here: https://dashboards.gitlab.net/d/Eo8BHoNZz/version-gitlab-com?orgId=1&refresh=5m&from=1570329167820&to=1571106767820

## Logging

All logging for the GKE cluster and all of its service is handled by
StackDriver.

The old environment does not have centralized log shipping, so we are not losing any functionality with this move. 


## Licensing

The version application is an internally developed GitLab application. It does not currently have a `LICENSE` file in [its repository](https://gitlab.com/gitlab-services/version-gitlab-com)

## Readiness review participants

1. @devin
1.
