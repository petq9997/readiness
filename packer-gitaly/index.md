# Packer-based rebuild process for gitaly

## Summary

This Production Readiness Review covers a change in how we are rebuilding VMs for the purpose of upgrading the underlying OS.

The scope of this rollout is limited to the gitaly fleet. The process is generic enough that it could be expanded to other parts of the fleet in the future (haproxy, postgres, redis), but we are focusing on gitaly for now.

See also epic [&602](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/602).

The main motivation is to reduce unavailability of gitaly during maintenance by a factor of 4, from 13 minutes to 3 minutes per node.

We accomplish this by speeding up the end-to-end latency of the node rebuild process by baking an image with packer.

## Background

Most of our gitaly fleet is deployed without clustering and HA. Thus, an outage of a single gitaly node will make all repositories hosted on that node unavailable.

We experience this unavailability during host failures from GCP, and we also experience it during any maintenance that requires a reboot.

This includes OS upgrades, which require terminating the instance and rebuilding it based on the new OS image.

Speeding up this process directly translates to less unavailability during this operation. Thus, we focus on making the critical section faster.

## Architecture

### Full rebuild (legacy process)

```mermaid
graph TB
  subgraph "Full rebuild of a VM"
  Delete[Delete existing instance] -- 1 min --> Create[Create new instance on stock ubuntu 20.04 image]
  Create -- 1 min --> Boot1[Initial boot, mount disks, upgrade kernel, reboot]
  Boot1 -- 1 min --> Boot2[Second boot, bootstrap via chef, install omnibus]
  Boot2 -- 11 min --> Done
end
```

### Packer rebuild (new process)

```mermaid
graph TB
  subgraph "Packer rebuild of a VM"
  HaltDeploys[Halt deployments] --> Build[Build image via packer]
  Build -- 30 min --> Packertest[Run packertest]
  Packertest -- 5 min --> Delete[Delete existing instance]
  Delete -- 1 min --> Create[Create new instance on packer-built image]
  Create -- 1 min --> Boot1[Initial boot, mount disks, reboot]
  Boot1 -- 1 min --> Boot2[Second boot, converge via chef]
  Boot2 --> Done
end
```

Components:

- **Packer:** Tool for building cloud images. We run it in a CI pipeline.
- **Packertest:** Our integration test that we use to verify a packer image is working by rebuilding an instance which is not receiving any traffic.

For more details, see [the docs in `config-mgmt`](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/blob/master/packer/README.md).

## Operational Risk Assessment

This is a significant change to how we (re-)provision machines. It introduces many risks, most of which we have worked to address or mitigate.

- Drift between old and new image building processes
  - Risk: Since we have several different processes, they may diverge and create subtle inconsistencies in the infrastructure.
  - Mitigation: The new process is designed to be as close as possible to the existing one. Images are built using the same bootstrap script that is used for provisioning. This ensures drift is kept to a minimum.
- Residue left on packer images during the image building process
  - Risk: Because images are built on ephemeral machines, we may persist this ephemeral hostname to the filesystem.
  - Mitigation: We mitigate this in two ways. For the cases where we have identified hostnames being persisted, we disable those sections in chef. We also perform a chef run right after booting from the image which leaves a very short delta of potentially wrong hostnames.
- Interference with production fleet or deploy process
  - Risk: While we build the image, we may cause interference with the production environment or the deploy process -- since we are running image builds in production.
  - Mitigation: To avoid interference with ongoing deployments, we coordinate with delivery to halt deployments during the maintenance.
- Stale images
  - Risk: With an image based approach, the image could be stale, containing an old version of omnibus.
  - Mitigation: We building a fresh image right before deploying it.
- Unexpected failures
  - Risk: There is always potential for unknown aspects to fail.
  - Mitigation: We have a few failsafes in place. We snapshot the old disks at the beginning of the process. We run a full rebuild of the "packertest" node before rebuilding production machines. Since this process is an optimization of the old one, should something specific to the process fail, we have the ability to fall back to the legacy full-rebuild process -- at an availability cost.
- Operator error
  - Risk: With any cricical procedure there is some risk of operator error.
  - Mitigation: The process is designed around automation, so it is as hands-off as possible. This avoids problems with copy-pasting, missing commands, running commands out-of-order. The automation has an idempotent design, this allows the process to be restarted should it break at any point. This aims to avoid the need to deviate from the automation in an emergency.
  - Mitigation: For the upcoming Gitaly OS Upgrade to Ubuntu 20.04 we will have additional SRE and EM coverage available synchronously during the maintenance windows.
We also reduce the risk of correlated failures by performing this change slowly in batches.

Each batch has a dedicated maintenance window.

## Database

n/a

## Security and Compliance

The images we build do contain secrets, as they are built via chef. Given the sensitivity of images and disk snapshots in general, we have protections restricting access to images and snapshots in place.

## Performance

This rebuild approach (packer-based) requires 3 minutes of downtime per node. This is faster than the full rebuild approach which takes 13 minutes of downtime per node. 

## Backup and Restore

Data disk is snapshotted regularly via the regular backup process.

The OS disk is snapshotted for emergency restore.

The standard revert process however is not to restore from backup, but to rebuild the node using the legacy rebuild process. That rollback procedure was tested extensively in staging.

## Monitoring and Alerts

n/a

## Responsibility

- Primary owner: @alejandro
- Secondary owner: @igorwwwwwwwwwwwwwwwwwwww

## Testing

Extensive testing of this process has been performed on the staging environment:

- https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/1421

We also run a `packertest` rebuild at the beginning of every batch to validate the freshly built image.
