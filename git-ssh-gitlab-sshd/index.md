# SSH services via GitLab SSHD (still in Kubernetes)

## Summary

This work follows from the previous PRR to move SSH services into Kubernetes:
[gitlab-shell on Kubernetes](../git-ssh-kubernetes/index.md)

`gitlab-sshd` is a new binary in the [gitlab-shell](https://gitlab.com/gitlab-org/gitlab-shell)
project which runs as a persistent SSH daemon. It will replace `OpenSSH` on
GitLab SaaS and eventually other cloud-native environments. Instead of running
an `sshd` process, we would run a `gitlab-sshd` process that does the same job,
in a more focused manner.

This feature is initially rolled out on GitLab SaaS only. This will also support the enablement of **Group IP address restriction**, but this will be rolled out separately via [Enable PROXY protocol on production](https://gitlab.com/gitlab-org/gitlab-shell/-/issues/548).

## Metrics

- [Apdex](https://dashboards.gitlab.net/d/git-main/git-overview?orgId=1) for the gitlab-shell service.
- [SSH performance metrics](https://dashboards.gitlab.net/d/-UvftW1iz/ssh-performance?orgId=1)
- [Resources](https://dashboards.gitlab.net/d/kubernetes-resources-pod/kubernetes-compute-resources-pod?orgId=1&refresh=5m) used to provide the gitlab-shell service.
- Thanos metrics for monitoring memory consumptions on [production](https://thanos-query.ops.gitlab.net/graph?g0.expr=container_memory_working_set_bytes%7Benv%3D%22gprd%22%2C%20namespace%3D%22gitlab%22%2C%20container%3D%22gitlab-shell%22%2C%20container!%3D%22%22%2C%20image!%3D%22%22%7D&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D) and [canary](https://thanos-query.ops.gitlab.net/graph?g0.expr=container_memory_working_set_bytes%7Benv%3D%22gprd%22%2C%20namespace%3D%22gitlab-cny%22%2C%20container%3D%22gitlab-shell%22%2C%20container!%3D%22%22%2C%20image!%3D%22%22%7D&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
- GitLab Shell [Pod Info](https://dashboards.gitlab.net/d/git-shell-pod/git-gitlab-shell-pod-info?orgId=1&var-PROMETHEUS_DS=Global&var-environment=gprd&var-cluster=gprd-us-east1-b&var-stage=main&var-namespace=gitlab&var-Node=All&var-Deployment=gitlab%3Fgitlab-shell)
- Adoption of group IP restriction feature on GitLab.com

## Related Epics
- [Implement gitlab-sshd on GitLab SaaS](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/603)
- [Make gitlab-sshd beta-quality](https://gitlab.com/groups/gitlab-org/-/epics/5219)
- [Make gitlab-sshd production ready on GitLab SaaS](https://gitlab.com/groups/gitlab-org/-/epics/6296)

## Architecture

### Current (OpenSSH + gitlab-shell)

```mermaid
sequenceDiagram
    participant Git on client
    participant SSH server
    participant AuthorizedKeysCommand
    participant GitLab Shell
    participant Rails
    participant Gitaly
    participant Git on server

    Note left of Git on client: git fetch
    Git on client->>+SSH server: ssh git fetch-pack request
    SSH server->>+AuthorizedKeysCommand: gitlab-shell-authorized-keys-check git AAAA...
    AuthorizedKeysCommand->>+Rails: GET /internal/api/authorized_keys?key=AAAA...
    Note right of Rails: Lookup key ID
    Rails-->>-AuthorizedKeysCommand: 200 OK, command="gitlab-shell upload-pack key_id=1"
    AuthorizedKeysCommand-->>-SSH server: command="gitlab-shell upload-pack key_id=1"
    SSH server->>+GitLab Shell: gitlab-shell upload-pack key_id=1
    GitLab Shell->>+Rails: GET /internal/api/allowed?action=upload_pack&key_id=1
    Note right of Rails: Auth check
    Rails-->>-GitLab Shell: 200 OK, { gitaly: ... }
    GitLab Shell->>+Gitaly: SSHService.SSHUploadPack request
    Gitaly->>+Git on server: git upload-pack request
    Note over Git on client,Git on server: Bidirectional communication between Git client and server
    Git on server-->>-Gitaly: git upload-pack response
    Gitaly -->>-GitLab Shell: SSHService.SSHUploadPack response
    GitLab Shell-->>-SSH server: gitlab-shell upload-pack response
    SSH server-->>-Git on client: ssh git fetch-pack response
```

### Proposed (gitlab-sshd)

```mermaid
sequenceDiagram
    participant Git on client
    participant GitLab SSHD
    participant Rails
    participant Gitaly
    participant Git on server

    Note left of Git on client: git fetch
    Git on client->>+GitLab SSHD: ssh git fetch-pack request
    GitLab SSHD->>+Rails: GET /internal/api/authorized_keys?key=AAAA...
    Note right of Rails: Lookup key ID
    Rails-->>-GitLab SSHD: 200 OK, command="gitlab-shell upload-pack key_id=1"
    GitLab SSHD->>+Rails: GET /internal/api/allowed?action=upload_pack&key_id=1
    Note right of Rails: Auth check
    Rails-->>-GitLab SSHD: 200 OK, { gitaly: ... }
    GitLab SSHD->>+Gitaly: SSHService.SSHUploadPack request
    Gitaly->>+Git on server: git upload-pack request
    Note over Git on client,Git on server: Bidirectional communication between Git client and server
    Git on server-->>-Gitaly: git upload-pack response
    Gitaly -->>-GitLab SSHD: SSHService.SSHUploadPack response
    GitLab SSHD-->>-Git on client: ssh git fetch-pack response
```

### Describe each component of the new feature and enumerate what it does to support customer use cases.

`gitlab-sshd` is the sole component. It is a drop-in replacement for the OpenSSH
+ `gitlab-shell` combination we are currently using, and performs exactly the same
duties - servicing requests over the SSH protocol. Those requests are:

* Git pull - `git-upload-pack`
* Git push - `git-receive-pack`
* Git archives - `git-upload-archive`
* Git LFS authentication - `git-lfs-authenticate`
* Personal access token management - `personal_access_token`
* Two-factor auth management - `2fa_recovery_codes` + `2fa_verify`
* GitLab username discovery - `discover`

In addition, unlike OpenSSH + `gitlab-shell`, `gitlab-sshd` has support for the
`PROXY` protocol. This is a prerequisite of making the "group IP restrictions"
feature work on GitLab SaaS ([gitlab#271673](https://gitlab.com/gitlab-org/gitlab/-/issues/271673)).

An alternative approach using a patched OpenSSH to solve this issue also exists,
but is not currently deployed ([CNG!657](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/657)),
because we have decided to [move forward](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/425) with
a more maintainable approach (`gitlab-sshd`).

## For each component and dependency, what is the blast radius of failures?

The blast radius is all requests over SSH. Possible failures include:

- Denial of service for all requests over SSH
- Credentials bypass leading to compromise of all repositories on GitLab.com
- Credentials bypass leading to compromise of two-factor auth and personal access tokens for all users on GitLab.com

It's a big change with potential for correspondingly big failures. In order to limit the number of users affected by the failures,
we plan to deploy the change gradually, i.e serve only a small percentage of traffic at first and then gradually increase the
percentage. In case a failure happens, gitlab-sshd can be rolled back.

## Is there anything in the feature design that will reduce this risk?

We currently use `gitlab-shell`, which **shares code** with `gitlab-sshd` - almost
all of the authentication and authorization code is shared. **All** of the command
implementation code is shared, although the command dispatch code is not shared.
This means that, practically, much of the `gitlab-sshd` implementation is already
tested and running in production. This reduces the areas we need to be concerned
about to the implementation of the SSH listener, the `gitlab-sshd`-specific
authorization, authentication, and command dispatch code, and anything in the
shared code that was safe in `gitlab-shell`, but cannot be safe in `gitlab-sshd`.

In that last category, a major functional departure between `gitlab-shell` and
`gitlab-sshd` is that in the former, each SSH connection occupied its own process.
In the latter, each occupies its own Goroutine. This presented risks during development,
as process-global state (particularly the process environment) was previously safe,
and is now unsafe. To mitigate this, we performed a review of the entire codebase
and resolved issues we found:
[gitlab-shell#496](https://gitlab.com/gitlab-org/gitlab-shell/-/issues/496) +
[gitlab-shell#501](https://gitlab.com/gitlab-org/gitlab-shell/-/issues/501).

Another functional departure is in how we handle onward connections to upstream
services - particularly the GitLab internal API, and Gitaly. In the former case,
we're moving from an HTTP connection per SSH connection, to a HTTP connection pool
[shared between all SSH connections](https://gitlab.com/gitlab-org/gitlab-shell/-/blob/master/internal/config/config.go#L100).
This should represent a reduction in load, and is safe due to the HTTP primitives
used. I discuss the latter case below.

An [AppSec security review](https://gitlab.com/gitlab-com/gl-security/appsec/appsec-reviews/-/issues/55)
has audited the code and its notable dependencies, particularly the
`golang.org/x/crypto/ssh` library. This found some minor problems which are now
fixed.

We also evaluated the risks of a single SSH connection causing degradation of the
whole service. The principal risk identified was a logic error -> panic in code
specific to a single SSH connection causing the whole daemon to shut down, so we
implemented a general recovery mechanism for this, analogous to the way Workhorse
(automatically, as part of the stdlib `net/http.Server`) handles a panic occurring
in any one HTTP request to prevent it from taking down the whole server:
[gitlab-shell#511](https://gitlab.com/gitlab-org/gitlab-shell/-/issues/511)

## Explain how this new feature will scale and any potential single points of failure in the design.

The `gitlab-sshd` binary is already available and configurable in the k8s pod we
use to deploy the `gitlab-shell` service to GitLab.com. In theory, switching to
it (and back) is a matter of changing a configuration variable and redeploying a
pod. All our existing k8s scalability gubbins is directly applicable to this
service.

We plan to perform a percentage-based rollout by having two fleets, one
configured for OpenSSH and one `gitlab-sshd`, and directing a variable amount of
traffic to the latter. The process is described in the [issue](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/6269).

Go is a garbage-collected language, but this doesn't always provide safety from
memory leaks. In a short-lived process handling a single connection like
`gitlab-shell`, those leaks are much less damaging than in a long-lived daemon
like `gitlab-sshd`. This is a scalability risk in the event of bugs, but it does
represent a scalability opportunity when running correctly - each SSH connection
in gitlab-sshd can use much less memory than in gitlab-shell, since the Go runtime's
costs are amortised across all connections, rather than being paid once per
connection. We would benefit from observing `gitlab-sshd` under production-like
load to gather a fair comparison of its behaviour w/rt memory use compared to
OpenSSH + `gitlab-shell`.

We create a separate Gitaly connection per SSH connection in `gitlab-shell`.
With `gitlab-sshd` we reuse Gitaly connections between SSH connections that
decreases pressure on Gitaly and avoid a scalability issue that could have
happened due to this problem.

## Operational Risk Assessment

### What are the potential scalability or performance issues that may result with this change?

A long-running daemon is more sensitive to the high memory usage and memory leaks.
For example, high-memory consumption of the `upload-pack via sidechannel` feature has been
[exposed](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/603#note_883599665) by
enabling `gitlab-sshd`. The issue has been fixed and additional performance tests has shown
that memory usage is lower [now](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/603#note_895423699).

### List the external and internal dependencies to the application (ex: redis, postgres, etc) for this feature and how it will be impacted by a failure of that dependency.

Dependency tree:

- GCP load-balancer
  - **`gitlab-sshd`**
    - GitLab internal API
      - Redis
      - Postgres
    - Gitaly
      - Filesystem

In general, this is unchanged from the existing scenario where we run OpenSSH +
`gitlab-shell`. Failure of any upstream dependency will cause an SSH request
going through `gitlab-sshd` to either hang or be terminated. While hanging, the
SSH request consumes resources - particularly RAM (CPU use is negligble). We
have seen issues with this on GitLab.com using OpenSSH + gitlab-shell in the
past.

Since we're moving from a process per connection to a goroutine per connection,
we should monopolise fewer resources than the status quo if a dependency enters
a degraded state.

### Were there any features cut or compromises made to make the feature launch?

There is a separate epic for work that we have judged can be left until after
feature launch: [gitlab-org&5394](https://gitlab.com/groups/gitlab-org/-/epics/5394).

### List the top three operational risks when this feature goes live.

- Performance/Scalability issues. One of the issues has been identified during our first attempt. The feature has been successfully rolled back and the issue has been [resolved](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/603#note_883599665). We plan to stick to this strategy if a similar problem happens again. We chose not to introduce limits to `gitlab-sshd` service, because it may cause availability issues when the limits are reached. We do have [a rate limit](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/78373) for Gitlab Shell operations per project/user. We plan to introduce [a rate limit by SSH](https://gitlab.com/gitlab-org/gitlab/-/issues/343103) when PROXY protocol is enabled.

- Prometheus metrics. With `gitlab-sshd` we are able to introduce Prometheus metrics and improve the monitoring of the service that provides SSH. However, we've been having issues recently due to the sheer number of metrics that our prometheus boxes have. Currently, the number of metrics is about 10 and should not cause any problem, but it is something to keep in mind anyway.

### What are a few operational concerns that will not be present at launch, but may be a concern later?

Nothing really comes to mind. The feature is a well-known quantity, and already
served by OpenSSH + `gitlab-shell`.

### Can the new product feature be safely rolled back once it is live, can it be disabled using a feature flag?

- The existing gitlab-shell container can be switched between the current and new state via Helm chart configuration
- The expected rollback duration is an [estimated 3 hours](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/6269#rollback) after the service is fully rolled out. If problems are detected along the way (such as what happened in the following [issue](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/6269)), rollback is nearly instant.
- It is not Feature Flagged. SSH connections are handled before any communication between `gitlab-shell` and GitLab Rails (the SSOT for feature flags) happens. That's why the traditional feature flag mechanism isn't applicable for rolling out this specific feature.

### Document every way the customer will interact with this new feature and how customers will be impacted by a failure of each interaction.

GitLab Shell is responsible for the [operations](https://gitlab.com/gitlab-org/gitlab-shell/-/blob/969bf2e27edf49c3998b62d64507c79909624e81/doc/features.md), performed via SSH. The most common ones are Git via SSH operations.
`gitlab-sshd` changes the way of handling SSH connections. If a failure happens, all of these mission-critical operations will be inaccessible.

### As a thought experiment, think of worst-case failure scenarios for this product feature, how can the blast-radius of the failure be isolated?

It is described in the `For each component and dependency, what is the blast radius of failures?` section. The risk is mitigated by the gradual rollout.

## Database

No additional database is used by `gitlab-sshd`, it interacts with Postgres+Redis via the GitLab internal API.

## Security and Compliance

### Were the [gitlab security development guidelines](https://about.gitlab.com/security/#gitlab-development-guidelines) followed for this feature?

Yes. Particularly see the AppSec review: https://gitlab.com/gitlab-com/gl-security/appsec/appsec-reviews/-/issues/55

### If this feature requires new infrastructure, will it be updated regularly with OS updates?

Mostly the same update arrangements and schedule as the status quo, particularly
around "OS updates".

We do drop a dependency on OpenSSH and gain a dependency on
`golang.org/x/crypto/ssh`. This moves the SSH listener code from "OS updates" to
"managed by GitLab". We manage the version of this dependency through
[`go.mod`](https://gitlab.com/gitlab-org/gitlab-shell/-/blob/main/go.mod) and
will need to keep on top of any security issues affecting it.

### Has effort been made to obscure or elide sensitive customer data in logging?

Yes. Much of the logging code is already deployed, via code shared with
gitlab-shell. Ongoing work to enhance logging is captured in
[gitlab-shell#499](https://gitlab.com/gitlab-org/gitlab-shell/-/issues/499) where
this is a consideration.

### Is any potentially sensitive user-provided data persisted? If so is this data encrypted at rest?

No

### Is the service subject to any regulatory/compliance standards? If so, detail which and provide details on applicable controls, management processes, additional monitoring, and mitigating factors.

There are some [concerns](https://gitlab.com/gitlab-com/gl-security/security-research/cryptographic-standards/-/issues/9) raised regarding FedRAMP and FIPS compliance: `gitlab-sshd` uses `golang.org/x/crypto` package which is not a FIPS-approved cryptographic module.
We've decided to move forward with `gitlab-sshd` deployment and [investigate](https://gitlab.com/gitlab-org/gitlab-shell/-/issues/545) later how we could make `gitlab-sshd` FIPS-compliant.
At the moment, the default configuration for FedRAMP customers should be `OpenSSH` and not `gitlab-sshd`.

## Performance

### Explain what validation was done following GitLab's [performance guidlines](https://docs.gitlab.com/ce/development/performance.html) please explain or link to the results below

    * [Query Performer](https://docs.gitlab.com/ce/development/query_recorder.html)
    * [Sherlock](https://docs.gitlab.com/ce/development/profiling.html#sherlock)
    * [Request Profiling](https://docs.gitlab.com/ce/administration/monitoring/performance/request_profiling.html)


A number of tests were [performed](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/603#note_883599665) on Staging and Staging Ref.
The memory consumption is now better for `gitlab-sshd` due to the running a single service that handles connections via Go goroutines.

We now have the ability to leverage Go Profiler which will be enabled when `gitlab-sshd` becomes enabled.
It will allow tracking CPU and memory bottlenecks.

### Are there any potential performance impacts on the database when this feature is enabled at GitLab.com scale?

There should be no changes to database load as a result of this change - the
rate at which we query the internal API will be unchanged.

### Are there any throttling limits imposed by this feature? If so how are they managed?

At the Pod level, with the current use of `gitlab-shell`, we utilize `MaxStartups` when a Pod
becomes overloaded with traffic. Our remediation to this is to always ensure we have enough Pods,
thus our scale, only takes into account CPU, which leads to a horrible disconnect between how many
Pods should run. Hopefully our scale value is set to something low enough where we do not exhaust
connections in our current form.

At the HAProxy level, we have no concept of distributing load to target Pods.
HAProxy doesn't have knowledge of how loaded this service is, it instead blindly sends a request
to the Service object of Kubernetes, which round robins the connections to the available Pods.
This is a representation of how we work today, thus there is technically no change to how this operates.
Is there risk in this? Yes. Currently, `gitlab-shell` will protect a Pod from being drowned by a lot of
slow requests, where-as it would seem like we lack this protection for gitlab-sshd.

When we switch to `gitlab-sshd` we chose not to have this mechanism because unauthorized SSH
connections are expected to consume fewer resources, while a limit may introduce a security risk.
Slow unauthorized requests can occupy a Pod restricting other clients from connecting due to
the limit. However, in order to clean up the idle connections, we have implemented an alternative
to `OpenSSH` LoginGraceTime option. If a user hasn't been able to be authorized within
a particular period of time (60 seconds by default, but can be configured), their connection is aborted.
We plan to closely monitor whether a limit must be introduced in order to avoid a Pod being overloaded.

There *is* an inbuilt concurrency limit per-connection. In general, we expect
only one concurrent "SSH session" per connection, the way we use SSH at gitlab.
The `ssh: concurrent_sessions_limit:` configuration key can be used to vary this;
it defaults to 10 concurrent sessions.

To play nicely with kubernetes, there are also inbuilt readiness and liveness
probes. We have also introduced graceful shutdown of the existing connections, so when
the service is terminated, it waits a particular period of time to have all the ongoing
connections completed (10 seconds by default, but can be configured to a bigger value).
When the shutdown initiated, readiness probe returns 503; otherwise, it returns 200.

### If there are throttling limits, what is the customer experience of hitting a limit?

In the event that someone decides to use a custom SSH client to multiplex lots
of concurrent SSH sessions (each one an independent `git fetch`, say), down a
single TCP connection, then after the tenth (by default) one, their custom SSH
client will start getting "resource exhausted" responses when it tries to
initiate an additional session. This is a standard SSH protocol response, so
should be gracefully handled.

### For all dependencies external and internal to the application, are there retry and back-off strategies for them?

Not within the context of a single SSH connection. An error communicating with
the internal API, or with Gitaly, will be faithfully reported back to the
client. At the daemon level, there are retry, but not back-off, semantics; as
far as I'm aware, this matches the status quo with OpenSSH + `gitlab-shell`. It
does mean that an error with the internal API (for instance) won't break SSH
connections occurring after the error has resolved.

### Does the feature account for brief spikes in traffic, at least 2x above the expected TPS?

Based on the performed performance evaluations, `gitlab-sshd` can handle at least the same
traffic that `gitlab-shell` is handling now. We expect it to handle brief spikes in traffic
as well.

## Backup and Restore

There no additional data store for `gitlab-sshd`

## Monitoring and Alerts

### Is the service logging in JSON format and are logs forwarded to logstash?

Yes; The logging configuration remains the same.

### Is the service reporting metrics to Prometheus?

`gitlab-sshd` has a prometheus listener which exposes a variety of metrics,
particularly around SSH request concurrency, and calls to the internal API and
gitaly, via the standard labkit interceptors.

This may require additional configuration in the production environment before
we can benefit from it, as this feature is not available when running OpenSSH +
`gitlab-shell`.  This is captured as improvement https://gitlab.com/gitlab-org/gitlab-shell/-/merge_requests/593 but is not a blocker for this rollout.  Reference: https://gitlab.com/gitlab-com/gl-infra/readiness/-/merge_requests/103#note_905518974

### How is the end-to-end customer experience measured?

[Apdex](https://dashboards.gitlab.net/d/git-main/git-overview?orgId=1&var-PROMETHEUS_DS=Global&var-environment=gstg&var-stage=main&viewPanel=2395479737) and [Requests per Second](https://dashboards.gitlab.net/d/git-main/git-overview?orgId=1&var-PROMETHEUS_DS=Global&var-environment=gstg&var-stage=main&viewPanel=315783164) panels for `gitlab-sshd`, [SSH Performance](https://dashboards.gitlab.net/d/-UvftW1iz/ssh-performance?orgId=1) panel.

### Do we have a target SLA in place for this service?

Yes - same as with the `OpenSSH + gitlab-shell` deployment it replaces

### Do we know what the indicators (SLI) are that map to the target SLA?

Yes, we have a [component](https://gitlab.com/gitlab-com/runbooks/-/blob/master/metrics-catalog/services/git.jsonnet#L253) that includes a request-rate to
notice traffic anomalies (no more connections, too many connections).

### Do we have alerts that are triggered when the SLI's (and thus the SLA) are not met?

We have [a component](https://gitlab.com/gitlab-com/runbooks/blob/bfccbbc35879fcc4ef8ad0790efdb784c7dcf95b/metrics-catalog/services/git.jsonnet#L278) that
includes an apdex SLI. It uses the alerting threshold set at the service level: [99.8%](https://gitlab.com/gitlab-com/runbooks/blob/bfccbbc35879fcc4ef8ad0790efdb784c7dcf95b/metrics-catalog/services/git.jsonnet#L24). We want 99.8% of connections to take within the specified durations (10s satisfied, 20s tolerated).
It's also worth noting that we have HAProxy sitting in front of this, which also has an SLI [here](https://gitlab.com/gitlab-com/runbooks/blob/bfccbbc35879fcc4ef8ad0790efdb784c7dcf95b/metrics-catalog/services/git.jsonnet#L110). This component does have an error rate (but no apdex), the SLO is 99.95%.

### Do we have troubleshooting runbooks linked to these alerts?

We have an issue to implement SLIs/SLAs to monitor error rates and durations on the connections:

- https://gitlab.com/gitlab-com/runbooks/-/issues/88

### What are the thresholds for tweeting or issuing an official customer notification for an outage related to this feature?

Any apdex violation. SSH outages would be highly visible to the userbase. SLIs are defined [here](https://gitlab.com/gitlab-com/runbooks/-/blob/master/metrics-catalog/services/git.jsonnet).

### do the oncall rotations responsible for this service have access to this service?

The EOC's most certainly [do](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/kube/k8s-oncall-setup.md). They would follow our existing runbooks for accessing our Kubernetes clusters for which this service is deployed in order to access the Pods that run the actual service.

## Responsibility

### Which individuals are the subject matter experts and know the most about this feature?

~"group::source code" members - particularly `@igor.drozdov` for gitlab-shell itself, and `@vyaklushin` w/rt the helm chart.

GitLab Shell maintainers - in addition to the two people above, `@patrickbajao` and `@ashmckenzie`

### Which team or set of individuals will take responsibility for the reliability of the feature once it is in production?

~"group::source code"

### Is someone from the team who built the feature on call for the launch? If not, why not?

@igor.drozdov @sean_carroll

## Testing

- Tested on preprod: https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/14453#note_799715646
- Tested on Staging: https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/14454#note_830426791

### Describe the load test plan used for this feature. What breaking points were validated?

The initial performance testing on Staging hasn't discovered any issues. After `gitlab-sshd` has been deployed on canary,
high memory consumption has been [identified](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/6269#note_837369407).
More aggressive load testing on Staging Ref and Staging could reproduce [the issue](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/603#note_883599665).
The necessary optimizations are [provided](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/603#note_895423699).
We expect the resources consumption by `gitlab-sshd` to be lower that the one by `gitlab-shell`.

### For the component failures that were theorized for this feature, were they tested? If so include the results of these failure tests.

- Potential scalability failures have been tested within Performance Review
- Potential security failures have been tested within Security Review

### Give a brief overview of what tests are run automatically in GitLab's CI/CD pipeline for this feature?

gitlab-qa has SSH-related testing. Since `gitlab-sshd` has been already enabled on Staging,
QA from auto-deploys is now properly testing gitlab-sshd.

There are acceptance tests that work against a real Gitaly, real SSH client, and
a stubbed internal API, in the gitlab-shell repository. These validate the
**correctness** of responses to well-formed SSH requests, but are necessarily
non-exhaustive.

As part of bringing gitlab-sshd to beta level, we evaluated unit test coverage
in the gitlab-shell repository and infilled areas that seemed lacking:
[gitlab-shell#498](https://gitlab.com/gitlab-org/gitlab-shell/-/issues/498).

GPT does not use `gitlab-sshd`, and we have no SSH=related coverage there to my
knowledge.
