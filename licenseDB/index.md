We are targeting this service for GA with the following caveats

- SREs in the Infrastructure Department will not have ownership over the service or Infrastructure
- There will not be weekend coverage or 24/7 response in case there is an issue unless the issue is specific to the Rails side
- We haven't yet incorporated this service into central monitoring, for GA we will be using alerts that will be sent to Slack and will be triaged by the service team.

## Note

* License-DB infrastructure has 2 environments. A `dev` environment mapping to `ext-license-db-dev-d6ba6f35` and a `prod` environment mapping to `ext-license-db-prd-43e2753d`.
* `dev` environment is used as a staging environment.
* Developers experiment in their sandbox environments.
* `dev` and `prod` are identical environments when it comes to infrastructure. Scheduled jobs run only on `prod`.
*
## Experiment

### Service Catalog

_The items below will be reviewed by the Reliability team._

- [x] **Link to the [service catalog entry](https://gitlab.com/gitlab-com/runbooks/-/tree/master/services) for the service. Ensure that the following items are present in the service catalog, or listed here:** [Entry for LicenseDB](https://gitlab.com/gitlab-com/runbooks/-/blob/0cadc63f879a87761a5da5447c197f6267247cdd/services/service-catalog.yml#L250-264)

  - **Link to or provide a high-level summary of this new product feature.**

    [High Level Summary](https://gitlab.com/gitlab-org/security-products/license-db/deployment/-/blob/b8a4ae01dc21309b4e92bdf0d019f1432bc53985/docs/DESIGN.md#summary)

  - **Link to the [Architecture Design Workflow](https://about.gitlab.com/handbook/engineering/architecture/workflow/) for this feature, if there wasn't a design completed for this feature please explain why.**

    This feature was initially developed by a different team and without following the Architecture Design Workflow. Though, some information on the architecture is available in [the project documentation](https://gitlab.com/gitlab-org/security-products/license-db/deployment/-/blob/main/docs/DESIGN.md).

  - **List the feature group that created this feature/service and who are the current Engineering Managers, Product Managers and their Directors.**

    Team: Secure Composition Analysis team ~"group::composition analysis"
    Engineering Manager: Olivier Gonzalez
    Product manager: Sara Meadzinger

  - **List individuals are the subject matter experts and know the most about this feature.**

    The subject matter experts for each feature are the following.

    - tchupryna, tchupryna@gitlab.com
    - Philip Cunningham, pcunningham@gitlab.com
    - fcatteau, fcatteau@gitlab.com
    - Igor Frenkel, ifrenkel@gitlab.com
    - nilieskou, nilieskou@gitlab.com

  - **List the team or set of individuals will take responsibility for the reliability of the feature once it is in production.**

    The composition analysis team is responsible for the reliability of the feature in production.

~"group::composition analysis"

  - **List the member(s) of the team who built the feature will be on call for the launch.**

    CA team does not have on call engineers but the team will respond on best-effort on slack during the launch.

  - **List the external and internal dependencies to the application (ex: redis, postgres, etc) for this feature and how the service will be impacted by a failure of that dependency.**

Below you can see a list of internal and external dependencies for this feature:

**Internal dependencies**

- Gitlab: We use Gitlab in order to run scheduled jobs for the feeder and exporter jobs. If Gitlab fails during the time schedule for which the feeder/exporter job needs to be executed then the exported data will be out of date. The impact is quite low since the data will eventually get updated within 24h if no manual action is taken.
- A variety of GCP services like GCP Cloud Storage, GCP Cloud SQL etc. For most of these services if they are down we won't be able to update the exported data resulting in customers having old data. Again the impact is quite low for the same reasons as for the Gitlab dependency.
- Gitlab/GCP registry: if any of the two services break then we wouldn't be able to deploy a new image/binary in order to fix a bug or ship a feature extension. Once more the impact would be small given the fact that these kind of outages should be fixed in short period of times.
- Gitlab Advisory DB: this is a Gitlab repo. If we cannot clone the repo then we won't be able to ingest new advisories. Customer will just not have the latest advisories. Impact is low.

**External dependencies**

Most of our external dependencies are the sources from which we fetch licenses. This can be in a form of an API server where we fetch data about packages or a git repo containing all the packages like for conan. If any external dependency fail then we won't be able to produce new data for that particular registry. For example if the [Golang](https://index.golang.org/index) registry fails then we won't be able to provide license information for new golang packages.

|           | Package information                                     | License information                                     |
|-----------|---------------------------------------------------------|---------------------------------------------------------|
| Golang    | https://index.golang.org/index                          | Downloads packages from Google's index, which itself downloads and caches them from the sources |
| Ruby      | https://rubygems.org/versions                           | https://rubygems.org/versions                           |
| Conan     | https://github.com/conan-io/conan-center-index.git      | https://github.com/conan-io/conan-center-index.git      |
| Maven     | Google's bucket mirror of Maven Central                 | Google's bucket mirror of Maven Central                 |
| Nuget     | https://api.nuget.org                                   | https://api.nuget.org                                   |
| NPM       | dev-couchdb-npm-mirror.org, prod-couchdb-npm-mirror.org | dev-couchdb-npm-mirror.org, prod-couchdb-npm-mirror.org |
| Pypi      | GCP BigQuery                                            | GCP BigQuery                                            |
| Packagist | https://repo.packagist.org/p2                           | https://packagist.org/packages/list.json                |

## Infrastructure

_The items below will be reviewed by the Reliability team._

**- [x] Do we use IaC (e.g., Terraform) for all the infrastructure related to this feature? If not, what kind of resources are not covered?**

All our infrastructure is provisioned using terraform. All changes are being deployed through CI/CD pipelines. The [deployment](https://gitlab.com/gitlab-org/security-products/license-db/deployment/-/tree/main/) project contains all the infrastructure related code.

The only exception is the NPM Couch DB instance that is currently not covered by IaaC. This [issue](https://gitlab.com/gitlab-org/gitlab/-/issues/413977) is a placeholder for the remaining IaaC work.

**- [x] Is the service covered by any DDoS protection solution (GCP/AWS load-balancers or Cloudflare usually cover this)?**

You can find more information about Network security in this [internal note](https://gitlab.com/gitlab-com/gl-infra/readiness/-/issues/66#note_1458123527)

- [x] **Are all cloud infrastructure resources labeled according to the [Infrastructure Labels and Tags](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/) guidelines?**

Yes all cloud infrastructure resources are labeled according to [Infrastructure Labels and Tags](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/). More specifically:

```
gl_realm = "eng-infra"
gl_env_type = "experiment"
gl_env_name = "eng-security"
gl_owner_email_handle = "nilieskou"
gl_dept = "eng-security"
gl_dept_group = "eng-dev-secure"
gl_product_category = "software_composition_analysis"
gl_data_classification = "green"
```

### Operational Risk

_The items below will be reviewed by the Reliability team._

- [x] **List the top three operational risks when this feature goes live.**

Overall the greatest operation risk is for the public GCP buckets containing license and advisory information not to be present because the data are missing or the GCP bucket itself cannot be reached. In this case a Gitlab instance would not be able to sync license and advisory information. Currently (milestone 16.2) advisory data are not being used by the Gitlab applications. Licenses information though are being used. Hence in case license information are not present, Gitlab would not be able to provide license information. That is the case for new Gitlab instances that try to sync for the first time with the Licenses GCP public bucket. Gitlab instances that have already synced at least once they will have license data but they won't be able to update the data. In this case the impact is low.

Moreover, there is the risk of overloading the rails platform with too much data. The public buckets contains data that come in deltas every 24 hours. If we export all the data again, a big delta can be created which will be more difficult for the backend to ingest. We have put a [throttle](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/115581) in place to ensure this would not canibalize the available resources.A huge amount of data will simply take more time to be processed and stored in the database. There is still a risk that the data will grow up to a point where the performance of the PostgreSQL DB will slow down, though this will be monitored like any other database table growth or SQL performance issue in the rails platform.

If trouble arises, for the short term we have development feature flags that allow to shut down the data sync or turn off the continuous scan on advisory ingestion. For the long term we also are considering to keep an [OPS flag](https://gitlab.com/gitlab-org/gitlab/-/issues/424669). There is currently no metrics available to the self-manage instance admins in the admin UI but this was [discussed for the long term](https://gitlab.com/gitlab-org/gitlab/-/issues/394747) so clients can get a hint on how things are doing for their instance.Overall this risk is low for the short term, and can still be refined the long term strategy.

- [ ] For each component and dependency, what is the blast radius of failures? Is there anything in the feature design that will reduce this risk?

### Monitoring and Alerting

_The items below will be reviewed by the Reliability team._

- [x] **Link to the [metrics catalog](https://gitlab.com/gitlab-com/runbooks/-/tree/master/metrics-catalog/services) for the service**

We currently do not send infrastructure related metrics to a central place (prometheus in combination with Grafana etc). Metrics are collected by GCP and we build alerts on top of them. Critical errors such as services not responding or resources being overwhelmed will send an email to email address provided in the [monitor terraform module](https://gitlab.com/gitlab-org/security-products/license-db/deployment/-/blob/main/modules/monitor/variables.tf) `notification_email_address` variable provided in the prod/dev.tfvars file. We currently alert on the following conditions:

- DB disk usage greater than 75%
- DB CPU usage greater than 75% for more than 300s
- DB Memory usage greater than 75% for more than 300s
- DB Excessive Deadlocks (10 in more than 60 seconds)
- PubSub messages being unacknowledged for more than 4 hours.
- Cloud Run Instance's CPU usage greater than 75% for more than 300s.
- Cloud Run Instance's Memory usage greater than 75% for more than 300s.

On top of sending alerts with emails we also have a slack channel (#g_secure-composition-analysis-alerts) dedicated to alerts. Every alert will be posted in the channel. You can find the alerts definitions in the [monitor](https://gitlab.com/gitlab-org/security-products/license-db/deployment/-/blob/main/modules/monitor/monitor.tf) terraform module.

Finally a sanity check of the exported data verifies that data is still present and updated regularly. Any failure will be reported to a dedicated slack channel where we monitor our projects tests failures: `#s_secure-alerts`.



### Deployment

_The items below will be reviewed by the Delivery team._

- [x] **Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.**

No, the infrastructure is already deployed in production, the existing service has been extended to provide capabilities for the new feature to be released.

- [x] **Can the new product feature be safely rolled back once it is live, can it be disabled using a feature flag?**

The feature logic on the rails application side sits behind a feature flag which rollout is handled via [[Feature flag] Rollout of `dependency_scanning_on_advisory_ingestion`](https://gitlab.com/gitlab-org/gitlab/-/issues/419550)

- [x] **How are the artifacts being built for this feature (e.g., using the [CNG](https://gitlab.com/gitlab-org/build/CNG/) or another image building pipeline).**

The new infrastracture consists of various micro-services that run either like a gitlab job ([license-feeder](https://gitlab.com/gitlab-org/security-products/license-db/license-feeder), [license-exporter](https://gitlab.com/gitlab-org/security-products/license-db/license-exporter)), either as a Cloud Run instance ([license-interfacer](https://gitlab.com/gitlab-org/security-products/license-db/license-interfacer),[license-processor](https://gitlab.com/gitlab-org/security-products/license-db/license-processor)). For the latter, the application is running as a docker instance in the Cloud Run, while in the formal case we just build a binary.

For all cases, binary or docker image, we have a release process in each project pipeline. For example, after we add a feature in one of the micro-services, a new release is being created in the pipeline. This release is available for use but not used yet.

We use the [deployment](https://gitlab.com/gitlab-org/security-products/license-db/deployment) project as the orchestrator. In this project we specify which [versions](https://gitlab.com/gitlab-org/security-products/license-db/deployment/-/blob/main/.gitlab-ci.yml?ref_type=heads#L31-46) of the projects we are using. By changing the version of a micro-service, a gitlab job will use the specified version in case of a binary release. In case of Cloud Run instances, the deployment project deploys the new release using the [deploy](https://gitlab.com/gitlab-org/security-products/license-db/deployment/-/blob/main/.gitlab-ci.yml?ref_type=heads#L12) jobs in the pipeline. These jobs are responsible for shipping the new release but also any change in infrastructure.

### Security Considerations

_The items below will be reviewed by the Infrasec team._

- [x] **Link or list information for new resources of the following type:**
  - AWS Accounts/GCP Projects:
  - New Subnets:
  - VPC/Network Peering:
  - DNS names:
  - Entry-points exposed to the internet (Public IPs, Load-Balancers, Buckets, etc...):
  - Other (anything relevant that might be worth mention):

You can find information about the GCP resources related to security in this [internal note](https://gitlab.com/gitlab-com/gl-infra/readiness/-/issues/66#note_1458114859)

- [x] **Were the [GitLab security development guidelines](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html) followed for this feature?**

Yes

- [x] **Do we have an automatic procedure to update the infrastructure (OS, container images, packages, etc...). For example, using unattended upgrade or [renovate bot](https://github.com/renovatebot/renovate) to keep dependencies up-to-date?**

Every infrastructure change is [deployed](https://gitlab.com/gitlab-org/security-products/license-db/deployment/-/blob/main/.gitlab-ci.yml?ref_type=heads#L11-12) through manual actions on pipelines running on the default branch. Terraform state is store using Gitlab as a backend. We currently do not use any dependency upgrade tool like renovate bot.


- [x] **For IaC (e.g., Terraform), is there any secure static code analysis tools like ([kics](https://github.com/Checkmarx/kics) or [checkov](https://github.com/bridgecrewio/checkov))? If not and new IaC is being introduced, please explain why.**

We recently [introduced](https://gitlab.com/gitlab-org/security-products/license-db/deployment/-/blob/main/.gitlab-ci.yml?ref_type=heads#L84) static analyzers (kics-iac-sast) checks on the terraform code.


- [x] **If we're creating new containers (e.g., a Dockerfile with an image build pipeline), are we using `kics` or `checkov` to scan Dockerfiles or [GitLab's container](https://docs.gitlab.com/ee/user/application_security/container_scanning/#configuration) scanner for vulnerabilities?**

Yes we do this for all projects that contain a docker image:
* [Schema project](https://gitlab.com/gitlab-org/security-products/license-db/schema/-/blob/main/.gitlab-ci.yml?ref_type=heads#L27)
* [Advisory-processor](https://gitlab.com/gitlab-org/security-products/license-db/advisory-processor/-/blob/main/.gitlab-ci.yml?ref_type=heads#L64)
* [License-interfacer](https://gitlab.com/gitlab-org/security-products/license-db/license-interfacer/-/blob/main/.gitlab-ci.yml?ref_type=heads#L25)
* [License-processor](https://gitlab.com/gitlab-org/security-products/license-db/license-processor/-/blob/main/.gitlab-ci.yml?ref_type=heads#L28)

### Identity and Access Management

_The items below will be reviewed by the Infrasec team._

- [x] **Are we adding any new forms of Authentication (New service-accounts, users/password for storage, OIDC, etc...)?**

You can find more information about Identity and Access Management in this [internal note](https://gitlab.com/gitlab-com/gl-infra/readiness/-/issues/66#note_1458119946). Please notice that since we wrote that post we have also [implemented OIDC](https://gitlab.com/gitlab-com/gl-infra/readiness/-/issues/66#note_1490395252) as an authentication mechanism so no service account keys are used anymore.

- [x] **Was effort put in to ensure that the new service follows the [least privilege principle](https://en.wikipedia.org/wiki/Principle_of_least_privilege), so that permissions are reduced as much as possible?**

You can find more information about Identity and Access Management in this [internal note](https://gitlab.com/gitlab-com/gl-infra/readiness/-/issues/66#note_1458119946). Please notice that since we wrote that post we have also [implemented OIDC](https://gitlab.com/gitlab-com/gl-infra/readiness/-/issues/66#note_1490395252) as an authentication mechanism so no service account keys are used anymore.

- [x] **Do firewalls follow the least privilege principle (w/ network policies in Kubernetes or firewalls on cloud provider)?**

You can find more information about Identity and Access Management in this [internal note](https://gitlab.com/gitlab-com/gl-infra/readiness/-/issues/66#note_1458119946). Please notice that since we wrote that post we have also [implemented OIDC](https://gitlab.com/gitlab-com/gl-infra/readiness/-/issues/66#note_1490395252) as an authentication mechanism so no service account keys are used anymore.

- [ ] **Is the service covered by a [WAF (Web Application Firewall)](https://cheatsheetseries.owasp.org/cheatsheets/Secure_Cloud_Architecture_Cheat_Sheet.html#web-application-firewall) in [Cloudflare](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/cloudflare#how-we-use-page-rules-and-waf-rules-to-counter-abuse-and-attacks)?**



### Logging, Audit and Data Access

_The items below will be reviewed by the Infrasec team._

- [x] Did we make an effort to redact customer data from logs?

Not Applicable

- [x] What kind of data is stored on each system (secrets, customer data, audit, etc...)?

The data that we store are mainly:
* Package and License data: Data is primarily made up of four pieces of information: package registry, package name, version and applicable licenses. In failure or error conditions, an error message may also be created and stored.
* Advisories: Data is made up of one table containing advisories from [GitLab Advisory Database](https://gitlab.com/gitlab-org/security-products/gemnasium-db) according to the defined [schema](https://gitlab.com/gitlab-org/security-products/gemnasium-db#yaml-schema).


- [x] **How is data rated according to our [data classification standard](https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html) (customer data is RED)?**

All license data ingested and stored in the database is public information taken from public package registries.
All advisory data ingested and stored in the database is information taken from the [GitLab Advisory Database](https://gitlab.com/gitlab-org/security-products/gemnasium-db). These data are already exposed in public buckets hence the data are categorised as green.


- [x] **Do we have audit logs for when data is accessed? If you are unsure or if using Reliability's central logging and a new pubsub topic was created, create an issue in the [Security Logging Project](https://gitlab.com/gitlab-com/gl-security/engineering-and-research/security-logging/security-logging/-/issues/new?issuable_template=add-remove-change-log-source) using the `add-remove-change-log-source` template.**

I am not sure this section is applicable since the data are public.

 - [x] **Ensure appropriate logs are being kept for compliance and requirements for retention are met.**

For the proposed features logs are used only for oeprational reasons. We do not have user interaction with the database. Only user interaction is accessing and downloading the contents of our license and advisory public buckets.

 - [x] **If the data classification = Red for the new environment, please create a [Security Compliance Intake issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/security-compliance-commercial-and-dedicated/security-compliance-intake/-/issues/new?issue[title]=System%20Intake:%20%5BSystem%20Name%20FY2%23%20Q%23%5D&issuable_template=intakeform). Note this is not necessary if the service is deployed in existing Production infrastructure.**

Not applicable

## Beta

### Monitoring and Alerting

_The items below will be reviewed by the Reliability team._

- [x] Link to examples of logs on https://logs.gitlab.

We are currently working in exporting logs to the Elastic instances as part of [Enable logs ingestion by Reliability team](https://gitlab.com/gitlab-org/gitlab/-/issues/426217)

- [x] Link to the [Grafana dashboard](https://dashboards.gitlab.net) for this service.

We are not going into GA with central dashboards or alerting for 24/7 oncall. We are planning to export metrics and alerts to the Reliability team after `GA`.

### Backup, Restore, DR and Retention

_The items below will be reviewed by the Reliability team._

- [x] **Are there custom backup/restore requirements?**

We do not have backup requirements for our Cloud SQL Postgres database. The reason is that we can reproduce all data by running the [license-feeder](https://gitlab.com/gitlab-org/security-products/license-db/deployment/-/blob/main/.gitlab-ci.yml?ref_type=heads#L266-286) and [advisory-feeder](https://gitlab.com/gitlab-org/security-products/license-db/deployment/-/blob/main/.gitlab-ci.yml?ref_type=heads#L266-286) jobs. However, we still have the automated backups offered by Cloud SQL on a daily basis.

- [x] **Are backups monitored?**

Not Applicable

- [x] **Was a restore from backup tested?**

Not Applicable since we can reproduce the data. What we have tested is that we can reproduce the data.

- [x] **Link to information about growth rate of stored data.**


The license-db contains data about packages, licenses and advisories for a variety of package managers. The largest part of the data is licenses and packages. Unfortunately it is hard to calculate the growth of these data through time. In order to do that we would need to dive into each public registry and extrapolate their respective growth rate.

The smallest part of the license-db contains advisory data. The size and the growth of these data is quite small. Right now we support the `Gitlab Advisory DB` which grew from `77Mb` in April 2020 to `143MB` in June 2023. We are planning to add as an advisory source the `Trivy vulnerability DB` which according to its official [documentation](https://aquasecurity.github.io/trivy/v0.17.2/examples/db/) the size is around `10~30MB` and in reality this will take around `200MB` in the database. Additionally the growth rate of CVEs can be found [here](https://www.cve.org/About/Metrics).

In total our database is less than 20GBs. Just for an example from June to September of 2023 we went from `15,3GB` to `16,6GB`. The growth of data due to our sources is not predicted to have any big spikes. Ingesting new sources might take some space but is not going to happen without first ironing the details.

### Deployment

_The items below will be reviewed by the Delivery team._

- [x] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.

No, the infrastructure is already deployed in production, the existing service has been extended to provide capabilities for the new feature to be released.

- [x] Does this feature have any version compatibility requirements with other components (e.g., Gitaly, Sidekiq, Rails) that will require a specific order of deployments?

The license-db infrastructure does not depend on other components. The sync lives in a Sidekiq worker that runs peridiocally, and it only interacts with License DB exports, and with gitlab's Postgres DB through Rails models. License Scanning scans happen in the Rails backend, and they interact with the same models. There should be no compatibility issues with respect to License DB exports.

We release a new export versions before it's being consumed by the sync. So far we've kept previous export versions for backward compatibility.

As for the DB schema and the Rails models, we have guidelines to ensure that migrations are compatible with mixed deployments.

- [x] Is this feature validated by our [QA blackbox tests](https://gitlab.com/gitlab-org/gitlab-qa)?

License-db is only validate by the [sanity checks](https://gitlab.com/gitlab-org/security-products/tests/license-db-sanity) which verifies size and freshness of the available data. All downstream workflows are tested within the context of a rails app with its local data (PG database).

- [x] Will it be possible to roll back this feature? If so explain how it will be possible.

The feature logic on the rails application side sits behind a feature flag which rollout is handled via [[Feature flag] Rollout of `dependency_scanning_on_advisory_ingestion`](https://gitlab.com/gitlab-org/gitlab/-/issues/419550). Futhermore, we are considering an [Ops Flag](https://gitlab.com/gitlab-org/gitlab/-/issues/424669) which will immediately stop any long-running advisory scanning job.

### Security

_The items below will be reviewed by the InfraSec team._

- [ ] Put yourself in an attacker's shoes and list some examples of "What could possibly go wrong?". Are you OK going into Beta knowing that?
- [ ] Link to any outstanding security-related epics & issues for this feature. Are you OK going into Beta with those still on the TODO list?

## General Availability

### Monitoring and Alerting

_The items below will be reviewed by the Reliability team._

- [x] Link to the troubleshooting runbooks.

You can find the license-db runbooks [here](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/external_license_db?ref_type=heads)

- [x] Link to an example of an alert and a corresponding runbook.

This [MR](https://gitlab.com/gitlab-org/security-products/license-db/deployment/-/merge_requests/182) introduces runbooks for all the available alerts.


- [x] Confirm that on-call engineers have access to this service.

NA. License-db infra is maintained by the `Secure CA` team.

### Operational Risk

_The items below will be reviewed by the Reliability team._

- [x] Link to notes or testing results for assessing the outcome of failures of individual components.

The main components of license-db infrastructure are responsible for feeding the ELD with data (licenses/advisories) and then exporting them to the public buckets. If any of these components stops working the failure radius is small since there will be available data in the public bucket. The consequence is that we won't have new data in the public buckets. User's will still be able to use the feature but without the latest data. So for example if a component stops working today, the users will still have license and advisory data available for up to including yesterday, since we export data once per day.


- [x] What are the potential scalability or performance issues that may result with this change?

The major impact would be to the monolith's database transactions per second and memory use. This is isolated to the `pm_` tables and constrained by the methods listed above. A potentially significant impact on customer experience would be data staleness. For large datasets, license data may not be up to date until the full dataset is processed. This is especially prominent on initial import and using version_format `v1` where a full sync could take multiple hours. We currently use `v2` format which has a significant smaller footprint.

- [x] What are a few operational concerns that will not be present at launch, but may be a concern later?

NA.

- [x] Are there any single points of failure in the design? If so list them here.

Overall the greatest operation risk(single point of failure) is for the public GCP buckets containing license and advisory information not to be present because the data are missing or the GCP bucket itself cannot be reached. In this case a Gitlab instance would not be able to sync license and advisory information. Hence in case license/advisory information are not present, Gitlab would not be able to provide license/advisory information. That is the case for new Gitlab instances that try to sync for the first time with the Licenses/Advisory GCP public bucket. Gitlab instances that have already synced at least once they will have license data but they won't be able to update the data. In this case the impact is low.

- [x] As a thought experiment, think of worst-case failure scenarios for this product feature, how can the blast-radius of the failure be isolated?

Please look on the previous answer.

### Backup, Restore, DR and Retention

_The items below will be reviewed by the Reliability team._

- [x] Are there any special requirements for Disaster Recovery for both Regional and Zone failures beyond our current Disaster Recovery processes that are in place?

The are not special requirements for Disaster recovery. Our [runbooks](https://gitlab.com/gitlab-org/security-products/license-db/deployment/-/tree/main/docs/runbooks?ref_type=heads) illustrate how to restore both ELD and public buckets. Currently we are not setting [RTO](https://internal.gitlab.com/handbook/engineering/disaster-recovery/#fy24-rto-and-rpo-targets) targets for this service for regional failures.

- [x] How does data age? Can data over a certain age be deleted?

License and advisory data cannot be deleted since they are always relevant. License and advisory information might be modified. Be aware that the footprint of these data is relatively small. Please look at **Link to information about growth rate of stored data.** section.

### Performance, Scalability and Capacity Planning

_The items below will be reviewed by the Reliability team._

- [ ] Link to any performance validation that was done according to [performance guidelines](https://docs.gitlab.com/ee/development/performance.html).
- [ ] Link to any load testing plans and results.
- [x] Are there any potential performance impacts on the Postgres database or Redis when this feature is enabled at GitLab.com scale?

The major impact would be to the monolith's database transactions per second and memory use. This is isolated to the `pm_` tables and constrained by the methods listed above. A potentially significant impact on customer experience would be data staleness. For large datasets, license data may not be up to date until the full dataset is processed. This is especially prominent on initial import and using version_format `v1` where a full sync could take multiple hours. We currently use `v2` format which has a significant smaller footprint.

- [x] Explain how this feature uses our [rate limiting](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/rate-limiting) features.

Not applicable since License DB doesn't expose any API endpoint right now. It only serves exports through GCP public buckets, and these have their own [quotas](https://cloud.google.com/storage/quotas).

- [x] Are there retry and back-off strategies for external dependencies?

A data [checkpointing scheme](https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/app/services/package_metadata/sync_service.rb#L77) is used so that on timeouts or exceeded worker lifetime, the job will restart and sync will pick up at the last processed file.

- [x] Does the feature account for brief spikes in traffic, at least 2x above the expected rate?

There should be no spike in traffic since the amount of data consumed/upserted is always the same, but larger exported datasets will affect data freshness.

### Deployment

_The items below will be reviewed by the Delivery team._

- [x] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.

No, the infrastructure (license-db) is already deployed in production, the existing service has been extended to provide capabilities for the new feature to be released.

- [x] Are there healthchecks or SLIs that can be relied on for deployment/rollbacks?

When it comes to license-db we do not have any SLIs or healthchecks since this is a straight forward pipeline that is triggered once per day with a success or failure state. I don't think we can perform a healthcheck on any of the internal license-db components.


- [x] Does building artifacts or deployment depend at all on [gitlab.com](https://gitlab.com)?

Yes. License-db services are built and deployed in Gitlab CI/CD Pipelines.
