# Redis Registry Cache

## Summary

- [ ] **Provide a high level summary of this new product feature. Explain how this change will benefit GitLab customers. Enumerate the customer use-cases.**

The GitLab.com Container Registry does not cache data from the storage backend or the new metadata database. Until one year ago, we were using a per-instance in-memory cache for blob descriptors (storage backend data), but this has proved to be inefficient and problematic (as explained in [gitlab-com/gl-infra/delivery#1878](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1878)). As such we decided to disable it on GitLab.com.

It was always our intent to rely on caching and unlock the benefits it can, in theory, provide. One year ago, a conversation was started in [gitlab-org/container-registry#396](https://gitlab.com/gitlab-org/container-registry/-/issues/396) around using Redis for the Container Registry. A list of use cases for Redis was enumerated in this issue, including the problems to address, and the expected benefits.
By then, it was consensual that this would be the right thing to do. However, we were about to start the GitLab.com Container Registry upgrade/migration ([gitlab-org&5523](https://gitlab.com/groups/gitlab-org/-/epics/5523)), and adding one more component at the same time would bring additional risk and complexity. So we decided to postpone this.

Fast forward one year, the GitLab.com Container Registry upgrade is complete, and the migration is close to completion. Therefore, we resumed the conversation around this topic and made an official proposal/request in [gitlab-com/gl-infra/scalability#1708](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/1708). This issue includes an up to date list of intended use cases, their load estimates and expected benefits.

We have now decided to start with the first use case: caching. This is the least risky use case and one we can more easily implement and roll out gradually. To be more precise, we are starting with caching repository objects from the metadata database. This is the single most used query, and repository objects are never deleted and rarely updated, so it is a perfect use case for caching. To narrow the impact even more, we are going to start by setting/getting cached repository objects in a single API route, more precisely the new [Get repository details](https://gitlab.com/gitlab-org/container-registry/-/blob/master/docs-gitlab/api.md#get-repository-details) operation. This operation is not part of the image pull/push flow (critical path). It is used exclusively by Rails to determine the size of repositories for displaying in the UI (low criticality), and currently has a very low request rate ([source](https://thanos.gitlab.net/graph?g0.expr=sum(%0A%20%20%20%20rate(%0A%20%20%20%20%20%20registry_http_requests_total%7B%0A%20%20%20%20%20%20%20%20env%3D%22gprd%22%2C%0A%20%20%20%20%20%20%20%20route%3D%22%2Fgitlab%2Fv1%2Frepositories%2F%7Bname%7D%22%0A%20%20%20%20%20%20%7D%5B2d%5D%0A%20%20%20%20)%0A)&g0.tab=1&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)).

Caching will not change how customers interact with the service but, in theory, should provide a non-negligible decrease in request latencies. Internally, we expect the queries per second metric to drop significantly once the feature is fully rolled out (used across all API routes). In general, the first use case for caching is not intended to provide groundbreaking benefits but rather to pave the way for more impactful use cases and test both the registry implementation and the new Kubernetes Redis deployment with a simple and low-risk use case.


- [ ] **What metrics, including business metrics, should be monitored to ensure will this feature launch will be a success?**

We expect decreased latency for the registry server https://dashboards.gitlab.net/d/registry-main/registry-overview?orgId=1&from=now-3h&to=now&viewPanel=53. We also expect the feature to be stable and not impact negatively the error rate https://dashboards.gitlab.net/d/registry-main/registry-overview?orgId=1&from=now-3h&to=now&viewPanel=880786463

## Architecture

- [ ] **Add architecture diagrams to this issue of feature components and how they interact with existing GitLab components. Include internal dependencies, ports, security policies, etc.**
The Container Registry architecture, including the new Redis component, is shown below:

```mermaid
graph LR
   C((Client)) -- HTTP request --> R(Container Registry) -- Upload/download blobs --> B(Storage Backend)
   R -- Write/read metadata --> D[(Database)]
   R -- Query/resolve master and replicas hostnames --> RS[(Redis Sentinel)]
   R -- Write/read cache data --> RE[(Redis)]
```

The only new piece here is the Redis instance and its connection with the Container Registry. Within the initial use case scope, caching repository objects from the metadata database for the [Get repository details](https://gitlab.com/gitlab-org/container-registry/-/blob/master/docs-gitlab/api.md#get-repository-details) operation.

In this context, when receiving a request for a repository, e.g. `gitlab-org/gitlab/cng/gitlab-pages`, the registry will first do a `GET` lookup for this repository against Redis and only fallback to the metadata database in case the key does not exist in Redis. If the key does not exist on Redis, but the repository does exist on the database, the registry will `SET` the repository object in Redis. In case of failure communicating with Redis or decoding the cached data, the registry will handle it gracefully and bypass the cache without any expected user impact. In the case the application does not behave as expected and described here, in the worst case scenario, the registry will be unable to serve [Get repository details](https://gitlab.com/gitlab-org/container-registry/-/blob/master/docs-gitlab/api.md#get-repository-details) requests, which are low in criticality and only account for 0.2% of the registry API traffic ([source](https://thanos.gitlab.net/graph?g0.expr=(sum(%0A%20%20%20%20rate(%0A%20%20%20%20%20%20registry_http_requests_total%7B%0A%20%20%20%20%20%20%20%20env%3D%22gprd%22%2C%0A%20%20%20%20%20%20%20%20route%3D%22%2Fgitlab%2Fv1%2Frepositories%2F%7Bname%7D%22%0A%20%20%20%20%20%20%7D%5B1d%5D%0A%20%20%20%20)%0A)%0A%2F%20%0Asum(%0A%20%20%20%20rate(%0A%20%20%20%20%20%20registry_http_requests_total%7B%0A%20%20%20%20%20%20%20%20env%3D%22gprd%22%2C%0A%20%20%20%20%20%20%20%20method!%3D%22options%22%2C%0A%20%20%20%20%20%20%20%20code%3D~%22%5E2.*%7C307%7C404%22%0A%20%20%20%20%20%20%7D%5B1d%5D%0A%20%20%20%20)%0A))%20*%20100&g0.tab=1&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)).

The registry currently sets the TTL of keys to 6 hours. This value will remain hardcoded for now, but we intend to fine-tune it and then make it configurable later. For more details about the key/value formats used, see the corresponding [development documentation](https://gitlab.com/gitlab-org/container-registry/-/blob/2ec23a6470e32cdc52d57fe102ada03eb99858e8/docs-gitlab/redis-dev-guidelines.md).

In the mid/long-term plan, Redis will act as an L2 cache. We intend to add a per-instance in-memory cache that will act as L1 and be kept in sync with Redis. This setup will be ideal for cases where the cached data is requested multiple times per API operation and rarely changes, which is the case for repository objects. However, to keep things simple, we are starting with just Redis.

The Container Registry supports connecting to single Redis instances and Redis Sentinel. It is possible to define dial, read and write timeouts and enable TLS. For each connection, the registry maintains a connection pool, for which size, maximum lifetime and idle timeouts can be configured. See the [configuration documentation](https://gitlab.com/gitlab-org/container-registry/-/blob/2ec23a6470e32cdc52d57fe102ada03eb99858e8/docs/configuration.md#cache-1) for more details.

At boot time, the registry will attempt to connect to the configured Redis cache (if `redis.cache.enabled` is `true`) and log a warning in case of failure. A failure will only prevent the cache feature from being enabled. It will _not_ prevent the application from starting in case of failure, regardless of the reason.

Regarding observability, the registry will emit several new Prometheus metrics. This includes metrics about the connection pool, namely the number of pool hits, misses, and timeouts, as well as the number of total, iddle, and stale connections in the pool. Additionally, it will also emit latency histograms and error counters, both for single and pipelined commands. All emitted metrics include an `instance` label, which is set to `cache` for this first use case. This will allow us to partition metrics by Redis instance, which is especially important as we will need other instances for different purposes beyond caching. A sample metrics output can be seen [here](https://gitlab.com/gitlab-org/container-registry/-/merge_requests/1088).

The application side metrics can be visualized in the new [Grafana dashboard](<TODO (João): Create new "Container Registry / registry: Redis detail" dashboard once we get to pre-production and link it here>).

- [ ] **Describe each component of the new feature and enumerate what it does to support customer use cases.**

  - Sentinel allows Registry to determine which hosts is the master and what are the available replicas for the redis cluster, so it can route requests to the proper Redis host
  - Redis hosts will store and provide fast access to the cache data

- [ ] **For each component and dependency, what is the blast radius of failures? Is there anything in the feature design that will reduce this risk?**

  - For Sentinel, misconfiguration or fatal failure means Registry will not be able to configure its connection to Redis. At startup, the Registry code handles gracefully not being able to confiure a connection to Redis, logging an error but continuing execution and bypassing Redis entirely: https://gitlab.com/gitlab-org/container-registry/-/blob/aa39dfc6149fefad01db46b316f349d69c9d1fb1/registry/handlers/app.go#L256-262.
  - For Redis, misconfiguration or fatal failure means Registry will be unable to store and retreive cache data. At startup this will be handle gracefully, as with Sentinel failures. If fatal failures occur after Registry initially establishes a connection, Registry requests will fail and emit an error. Data corruption could also result in Registry errors. Data loss would mean Registry won't be able to take advantage of caching, but it should continue to operate properly.

- [ ] **If applicable, explain how this new feature will scale and any potential single points of failure in the design.**

Redis is deployed in a highly available manner using sentinel for service discovery and failovers. Replicas are deployed across zones, which is enforced via anti-affinity.

We are subject to the single-threaded bottleneck in Redis. We monitor and forecast this bottleneck via saturation framework SLIs and our tamland capacity planning process.

## Operational Risk Assessment

- [ ] **What are the potential scalability or performance issues that may result with this change?**
- [ ] **List the external and internal dependencies to the application (ex: redis, postgres, etc) for this feature and how the it will be impacted by a failure of that dependency.**

  - ExternalDNS will provide the hostnames that Registry will use. If it's not working properly, Registry will be unable to connect to Sentinel or Redis. see [its Readiness Review](https://gitlab.com/gitlab-com/gl-infra/readiness/-/merge_requests/119) for more details.

- [ ] **Were there any features cut or compromises made to make the feature launch?**

No major cut features.

We are still missing [log-level summaries for redis activity](https://gitlab.com/gitlab-org/container-registry/-/issues/739).

- [ ] **List the top three operational risks when this feature goes live.**

  - Sentinel/Redis is not configured properly. This is verifiable before launch by querying the sentinel nodes and checking that they are operating correctly, and connecting to the Redis service directly.
  - The Redis pods are not sized properly, leading to saturation which in turn could lead to increased latency and user-facing failures. This is greatly mitigated as only 0.2% of registry traffic uses the cache at launch.

- [ ] **What are a few operational concerns that will not be present at launch, but may be a concern later?**
- [ ] **Can the new product feature be safely rolled back once it is live, can it be disabled using a feature flag?**

Removing the Redis configuration for Registry in the gitlab-com repository will trigger a new deployment of the Registry application. This tends to take around 20 minutes (see [an example pipeline](https://ops.gitlab.net/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/pipelines/1331157)). There is no feature flag at the moment. If Redis is completely unavailable, a rolling restart is a quick way to get Registry to bypass caching entirely. This can be done with the following command: `kubectl -n gitlab rollout restart deploy gitlab-registry`

- [ ] **Document every way the customer will interact with this new feature and how customers will be impacted by a failure of each interaction.**

Customers will not directly interact with this feature. Data loss or failures when Registry attempts to first establish a Redis connection will mean the cache will not be effective and won't provide optimal latency reduction. At worst this means having the same latency levels as with the current status quo. Unhandled failures on existing Redis connections will result in user-facing errors.

- [ ] **As a thought experiment, think of worst-case failure scenarios for this product feature, how can the blast-radius of the failure be isolated?**

Redis or Sentinel fatal failures after Regitry establishes a connection on startup will result in increased error rate until Redis recovers. In case Redis/Sentinel is in a broken state, restarting Registry will allow it to continue serving requests by bypassing the cache entirely.

## Database

- [ ] **If we use a database, is the data structure verified and vetted by the database team?**

N/A

- [ ] **Do we have an approximate growth rate of the stored data (for capacity planning)?**

No, but this instance is configured as a LRU, which sets a soft limit on data size
(see next item)

- [ ] **Can we age data and delete data of a certain age?**

The data has a TTL of 6 hours (see https://gitlab.com/gitlab-org/container-registry/-/blob/aa39dfc6149fefad01db46b316f349d69c9d1fb1/registry/handlers/app.go#L76). Additionally, redis is configured as a LRU cache [using `maxmemory` settings](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/tanka-deployments/-/blob/d127b7960cc3897fb0fbde9057536d100bc4389d/environments/redis/values.jsonnet#L43-45)

## Security and Compliance

- [ ] **Were the [gitlab security development guidelines](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html) followed for this feature?**
- [ ] **If this feature requires new infrastructure, will it be updated regularly with OS updates?**

Yes, the `redis` chart [is annotated for periodic renovation](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/tanka-deployments/-/blob/cc2eda2e70f1c6c2d262318e09a8690f2fd56ae9/chartfile.yaml#L19-21)

- [ ] **Has effort been made to obscure or elide sensitive customer data in logging?**

No part of the cached data is logged.

- [ ] **Is any potentially sensitive user-provided data persisted? If so is this data encrypted at rest?**

The keys are repository names, the values are binary blobs. Repository names are also stored as plaintext in the
database, and are never advertised by this new component. Thus, we consider encryption not necessary.

- [ ] **Is the service subject to any regulatory/compliance standards? If so, detail which and provide details on applicable controls, management processes, additional monitoring, and mitigating factors.**

Standard change management process applies.

## Performance

- [ ] **Explain what validation was done following GitLab's [performance guidlines](https://docs.gitlab.com/ce/development/performance.html) please explain or link to the results below**
    * [Query Performer](https://docs.gitlab.com/ce/development/query_recorder.html)
    * [Sherlock](https://docs.gitlab.com/ce/development/profiling.html#sherlock)
    * [Request Profiling](https://docs.gitlab.com/ce/administration/monitoring/performance/request_profiling.html)
- [ ] **Are there any potential performance impacts on the database when this feature is enabled at GitLab.com scale?**

No major change, as only a single endpoint is using caching at launch. As we add more caching to the application, the load on the registry postgresql database should be reduced.

- [ ] **Are there any throttling limits imposed by this feature? If so how are they managed?**

No. The throttling mechanism for registry will limit access to this feature.

- [ ] **If there are throttling limits, what is the customer experience of hitting a limit?**

N/A

- [ ] **For all dependencies external and internal to the application, are there retry and back-off strategies for them?**

Yes. If attempting to retreive an item from the cache throws an error, [it is
retreived from the
backend](https://gitlab.com/gitlab-org/container-registry/-/blob/f02447d3702eabdaa58c35b173e99e737036c398/registry/storage/cache/cachedblobdescriptorstore.go#L60-74)

- [ ] **Does the feature account for brief spikes in traffic, at least 2x above the expected TPS?**

## Backup and Restore

- [ ] **Outside of existing backups, are there any other customer data that needs to be backed up for this product feature?**

The cache is considered ephemeral, so we do not require any persistence, and thus no backups.

Wiping the cache may have some short-term performance implications, but this should happen rarely, and recover quickly.

- [ ] **Are backups monitored?**

N/A

- [ ] **Was a restore from backup tested?**

N/A

## Monitoring and Alerts

- [ ] **Is the service logging in JSON format and are logs forwarded to logstash?**

Yes https://log.gprd.gitlab.net/goto/14b2fb70-0e75-11ed-8656-f5f2137823ba

- [ ] **Is the service reporting metrics to Prometheus?**

Yes https://thanos-query.ops.gitlab.net/graph?g0.expr=registry_redis_single_commands_bucket&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D

- [ ] **How is the end-to-end customer experience measured?**

Through Apdex scores, see:
- https://dashboards.gitlab.net/d/registry-redis/registry-redis-detail?orgId=1
- https://dashboards.gitlab.net/d/registry-main/registry-overview?orgId=1&var-PROMETHEUS_DS=Global&var-environment=gstg&var-stage=main

- [ ] **Do we have a target SLA in place for this service?**

Yes, added on https://gitlab.com/gitlab-com/runbooks/-/merge_requests/5036

- [ ] **Do we know what the indicators (SLI) are that map to the target SLA?**

Yes, see https://gitlab.com/gitlab-com/runbooks/-/merge_requests/5036

- [ ] **Do we have alerts that are triggered when the SLI's (and thus the SLA) are not met?**

Yes, added on https://gitlab.com/gitlab-com/runbooks/-/merge_requests/5036

- [ ] **Do we have troubleshooting runbooks linked to these alerts?**
- [ ] **What are the thresholds for tweeting or issuing an official customer notification for an outage related to this feature?**
- [ ] **do the oncall rotations responsible for this service have access to this service?**

## Responsibility

- [ ] **Which individuals are the subject matter experts and know the most about this feature?**

From the application side: @jdrpereira, @jaime

From the infrastructure side: @alejandro, @igorwwwwwwwwwwwwwwwwwwww

- [ ] **Which team or set of individuals will take responsibility for the reliability of the feature once it is in production?**
- [ ] **Is someone from the team who built the feature on call for the launch? If not, why not?**

## Testing

- [ ] **Describe the load test plan used for this feature. What breaking points were validated?**
- [ ] **For the component failures that were theorized for this feature, were they tested? If so include the results of these failure tests.**
- [ ] **Give a brief overview of what tests are run automatically in GitLab's CI/CD pipeline for this feature?**

